#!/usr/bin/env python

"""
setup.py file for RNS
"""

from distutils.core import setup, Extension

# Fixing build order
from distutils.command.build import build as _build

#Define custom build order, so that the python interface module
#created by SWIG is staged in build_py.
class build(_build):
    # different order: build_ext *before* build_py
    sub_commands = [('build_ext',     _build.has_ext_modules),
                    ('build_py',      _build.has_pure_modules),
                    ('build_clib',    _build.has_c_libraries),
                    ('build_scripts', _build.has_scripts),
                   ]

module = Extension('_RNS', ['src/bind.i'], 
                   swig_opts=['-modern', '-I./include', '-c++'], 
                   include_dirs=['include'],
                   libraries=['rns_client', 'protobuf'],
                   library_dirs=['lib'],
                   )

setup (name = 'RNS',
       version = '0.1',
       author      = "Dmitry Kargin",
       description = """Python bindings for RNS Client SDK""",
       cmdclass = {'build': build }, # Changing build order
       ext_modules = [module],
       py_modules = ['RNS'],
       package_dir = {'' : 'src'}
       )