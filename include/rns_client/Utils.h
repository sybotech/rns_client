///////////////////////////////////////////////////////////////////////////////////////
//
// IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
// By downloading, copying, installing or using the software you agree to this license.
// If you do not agree to this license, do not download, install,
// copy or use the software.
//
//
// License Agreement
// For RNS Client SDK
//
// Copyright (C) 2013-2014, Sybo Tech LLC, all rights reserved.
// Third party copyrights are property of their respective owners.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
// * Redistribution's of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// * Redistribution's in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
//
// * The name of the copyright holders may not be used to endorse or promote products
// derived from this software without specific prior written permission.
//
// This software is provided by the copyright holders and contributors "as is" and
// any express or implied warranties, including, but not limited to, the implied
// warranties of merchantability and fitness for a particular purpose are disclaimed.
// In no event shall the Intel Corporation or contributors be liable for any direct,
// indirect, incidental, special, exemplary, or consequential damages
// (including, but not limited to, procurement of substitute goods or services;
// loss of use, data, or profits; or business interruption) however caused
// and on any theory of liability, whether in contract, strict liability,
// or tort (including negligence or otherwise) arising in any way out of
// the use of this software, even if advised of the possibility of such damage.
//
//

#ifndef UTILS_H_
#define UTILS_H_

#ifdef _WIN32
#    ifdef RNS_CLIENT_EXPORTS
#        define RNS_CLIENT_API __declspec(dllexport)
#    else
#        define RNS_CLIENT_API __declspec(dllimport)
#    endif
#else
#    define RNS_CLIENT_API
#endif

// Head for intrusive double linked list
// List head and tail are connected to each other, empty node is linked to itself
// Typical foreach:
//		for(Node * node = firstNode->next(); node != firstNode; node = node->next())
//		{
//			...
//		}
// This structure allows to skip all checks for NULL, because pointers are always valid

template <class Type>
class RNS_CLIENT_API IntrusiveListNode
{
public:
	IntrusiveListNode()
	{
		prev = static_cast<Type*>(this);
		next = static_cast<Type*>(this);
	}

	virtual ~IntrusiveListNode()
	{
		unlink();
	}

	// Intrusive linked list part
	void unlink()
	{
		// unlink from list
		prev->next = next;
		next->prev = prev;
		// short-cirquit self
		prev = static_cast<Type*>(this);
		next = static_cast<Type*>(this);
	}

	void linkNext(Type * object)
	{
		assert(object != this);
		object->unlink();

		object->prev = static_cast<Type*>(this);
		object->next = next;

		next->prev = object;
		next = object;
	}

	void linkPrev(Type * object)
	{
		assert(object != this);
		object->unlink();

		object->next= static_cast<Type*>(this);
		object->prev = prev;

		prev->next = object;
		prev = object;
	}

	/// Check if object is isolated
	bool isolated() const
	{
		// Is it OK?
		return next != static_cast<const Type*>(this);
	}

	const Type * getNext() const
	{
		return next;
	}

	Type * getNext()
	{
		return next;
	}

	const Type * getPrev() const
	{
		return prev;
	}

	Type * getPrev()
	{
		return prev;
	}
protected:
	Type * prev;
	Type * next;
};


#endif /* UTILS_H_ */
