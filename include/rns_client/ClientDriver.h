///////////////////////////////////////////////////////////////////////////////////////
//
// IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
// By downloading, copying, installing or using the software you agree to this license.
// If you do not agree to this license, do not download, install,
// copy or use the software.
//
//
// License Agreement
// For RNS Client SDK
//
// Copyright (C) 2013-2014, Sybo Tech LLC, all rights reserved.
// Third party copyrights are property of their respective owners.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
// * Redistribution's of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// * Redistribution's in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
//
// * The name of the copyright holders may not be used to endorse or promote products
// derived from this software without specific prior written permission.
//
// This software is provided by the copyright holders and contributors "as is" and
// any express or implied warranties, including, but not limited to, the implied
// warranties of merchantability and fitness for a particular purpose are disclaimed.
// In no event shall the Intel Corporation or contributors be liable for any direct,
// indirect, incidental, special, exemplary, or consequential damages
// (including, but not limited to, procurement of substitute goods or services;
// loss of use, data, or profits; or business interruption) however caused
// and on any theory of liability, whether in contract, strict liability,
// or tort (including negligence or otherwise) arising in any way out of
// the use of this software, even if advised of the possibility of such damage.
//
//

#ifdef _WIN32
#    ifdef RNS_CLIENT_EXPORTS
#        define RNS_DRIVER_CLIENT_API __declspec(dllexport)
#    else
#        define RNS_DRIVER_CLIENT_API __declspec(dllimport)
#    endif
#else
#    define RNS_DRIVER_CLIENT_API
#endif


namespace RNS {

	/** @class Joystick
	Contains POD data about robots speeds^ forward (x_speed), strafe(y_speed) and rotation(angle_speed)
	*/
	struct Joystick {
		double x_speed, y_speed, angle_speed;
	};

	/** @class Odometry
	Contains POD data about robots forward (x), strafe(y) and rotation(angle)
	*/
	struct Odometry {
		double x, y, angle;
	};

	/** @class LaserInfo
	Contains data about laser rangefinder
	*/
	struct LaserInfo
	{
		double angle_min;
		double angle_max;
		double range_min;
		double range_max;
		double pose[3];
		int beams;
		int device_index;
	};

	/** @class RobotDriverClient
	Driver for commnication of joystick and odometry data with server
	*/
	class RNS_DRIVER_CLIENT_API RobotDriverClient{
	public:
		/** Constructor 
		@param newConnectAddr String with protocol, server address and port. For example: "tcp://localhost:6666" "tcp://10.0.0.1:6666"
		@param newSocketWaitTimeMilisec Delay in miliseconds time for all send or receive operations. 
		@param newDeadTimeMilisec Delay in miliseconds before driver decides that communication failed and begins returning zero speed for robot
		@param newReconnectTimeMilisec Delay in miliseconds before driver tries to reconnect to server
		*/
		RobotDriverClient(const char *newConnectAddr, int newSocketWaitTimeMilisec = 50, int newDeadTimeMilisec = 500, int newReconnectTimeMilisec = 5000);
		
		/** Connect socket to server
		*/
		void connectSocket();
		
		/** Manually reconnects socket
		*/
		void reconnectSocket();
		
		/** Receives velocity command from server
		@param j - reference to target velocity variable. It is set inside the function
		@return 0 if receive failed 1 if receive went ok, also 1 if received heartbeat, that means target velocity didn't change
		*/
		int receive(Joystick &j);
		
		/** Resends last odometry message if send result was 0
		@return 1 if no data to be resent is available and nothing was done, 1 if resending went ok, 0 if resending didn't succeed
		*/
		int resend();
		
		/** Sends odometry data
		@param newOdom - data about robot odometry that you provide to the driver
		@return 1 if seding went ok, 0 if send didn't succeed. 1 doesn't guarantee receiving of the message by the driver, only that it was queued to be sent.
		*/
		int send(const Odometry &newOdom);

		/** Sends laser ranger data
		@param info - laser description
		@param ranges - array of measured distances. It size should correspond to info.beams
		@return 1 if seding went ok, 0 if send didn't succeed. 1 doesn't guarantee receiving of the message by the driver, only that it was queued to be sent.
		*/
		int send(const LaserInfo & info, double * ranges);
	private:
		/** Class hiding implementation for not including ZMQ and protobuf in shipment bundle
		*/
		class Impl;

		/// Pointer to hidden implementation
		Impl *pi;
	};
}
