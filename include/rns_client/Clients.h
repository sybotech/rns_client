///////////////////////////////////////////////////////////////////////////////////////
//
// IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
// By downloading, copying, installing or using the software you agree to this license.
// If you do not agree to this license, do not download, install,
// copy or use the software.
//
//
// License Agreement
// For RNS Client SDK
//
// Copyright (C) 2013-2014, Sybo Tech LLC, all rights reserved.
// Third party copyrights are property of their respective owners.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
// * Redistribution's of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// * Redistribution's in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
//
// * The name of the copyright holders may not be used to endorse or promote products
// derived from this software without specific prior written permission.
//
// This software is provided by the copyright holders and contributors "as is" and
// any express or implied warranties, including, but not limited to, the implied
// warranties of merchantability and fitness for a particular purpose are disclaimed.
// In no event shall the Intel Corporation or contributors be liable for any direct,
// indirect, incidental, special, exemplary, or consequential damages
// (including, but not limited to, procurement of substitute goods or services;
// loss of use, data, or profits; or business interruption) however caused
// and on any theory of liability, whether in contract, strict liability,
// or tort (including negligence or otherwise) arising in any way out of
// the use of this software, even if advised of the possibility of such damage.
//
//


#ifndef RNS_CLIENTS_H_
#define RNS_CLIENTS_H_

#include <list>
#include <exception>
#include <string>

#include "Utils.h"

///\brief RNS namespace
/// Contains all RNS-related classes
namespace RNS
{
	class Client;
	class Connector;

	/// Network exception class
	class NetworkError : public std::exception
	{
		std::string msg;
		explicit NetworkError(const char * message)
		{
			msg = message != NULL ? message : "no msg";
		}

		explicit NetworkError(const std::string& message)
		:msg(message)
		{}

		virtual ~NetworkError() throw() {}

		//virtual ~NetworkError() {}

		virtual const char *what () const throw ()
		{
			return msg.c_str();
		}
	};

	/// Some methods can fail by reason or timeout
	enum CommandResult
	{
		CommandDone,		///< Command has been executed
		CommandFailed,		///< Failed to execute command
		CommandTimeout,		///< Timeout has occurred
		CommandError,		///< Some sort of error has occurred
	};

	///\brief Wrapper around ZMQRoutines subscribe/publish framework.
	/// It simplifies remote access to robot and provides basic methods, necessary for any client
	class RNS_CLIENT_API Client // : public IntrusiveListNode<Client>
	{
	public:
		virtual ~Client();
		/// Check if client is attached to connector
		/// @returns true if connection is valid
		bool valid() const;
		/// sleep for some time
		void sleep(int ms) const;
		/// wait until ... something happens
		CommandResult wait(int ms);
		/// check if new data is arrived during last update
		bool hasNewData() const;
		/// set max timeout for all blocking requests
		void setTimeout(int ms);
		/// Get assigned connector
		Connector * getConnector();
		/// Wait until ticket state changes, or timeout occurs
		CommandResult waitForTicket(int ticket, int ms = 0);
		/// Wait until ticket state changes, or timeout occurs
		CommandResult waitForTicketComplete(int ticket, int ms = 0);
		/// Check if client is attached to any connector
		bool attached() const;
		/// detach itself from the connector
		void detach();
	protected:
		/// Class is not designed for separate usage
		Client(Connector & connector);
		/// called by connector before reading any new packets
		void newTick(unsigned int tick);
		/// called by connector when all packets are received
		void finish();

		void setTaskTicket(int ticket);
		unsigned int ticket;
		/// Clears internal data.
		virtual void reset();

		Connector * connector;
		/// Internal implementation
		class Impl;

		virtual Impl * getImpl() = 0;

		friend class Connector;
		friend class ConnectorImpl;
	};

	/// Deals all connection/ZMQ-related stuff
	class RNS_CLIENT_API Connector
	{
	public:
		Connector();
		Connector(const char * robotUrl);
		virtual ~Connector();

		/// Connect to robot controller
		/// @param robotUrl DNS name or IP addres of the controller
		/// @param subPort port for ZMQ SUB socket
		/// @param reqPort port for ZMQ REQ socket
		/// @param pushPort port for ZMQ PUSH socket
		int connect(const char * robotUrl = "10.5.5.1", int subPort = 5557, int reqPort = 5559, int pushPort = 5558);

		/// Checks if connection is established
		bool connected() const;

		/// wait until connection is established
		/// @param timeMS timeout
		/// @returns CommandDone if connected, CommandTimeout if no connection has occured
		CommandResult waitForConnection(int timeMS = -1);
		/// Disconnect from controller
		void disconnect();

		class Impl;
		friend class Impl;

		Impl * getImpl();

		/// Detach a client
		void detach(Client * client);
		/// Attach a client
		bool attach(Client * client);
	protected:
		/// Detach all clients
		void detachAll();
		
		Impl * impl;
		friend class Client;
	};

	/// Information about camera device
	struct CameraInfo
	{
		int width;			///< Frame width
		int height;			///< Frame height
		int flags;			///< Additional flags
		int bpp;			///< Bytes per pixel
		int data_size;		///< Frame data size, in bytes
		float pose[3];		///< pose relative to robot kinematic center
		char fourcc[8];		///< format description
		std::string frame_id;
	};

	/// Information about range finder sensor
	struct RangerInfo
	{
		float rangeMin;
		float rangeMax;
		float angleMin;
		float angleMax;
		unsigned int beams;		///< number of beams
		float pose[3];			///< pose relative to robot kinematic center
		std::string frame_id;
	};

	/// Implementation for accessing ranger data
	class RNS_CLIENT_API SensorClient : public Client
	{
	public:
		/// Constructor
		SensorClient(Connector & connector);
		~SensorClient();
		/// Get number of available sensors
		int getRangersCount() const;
		/// read ranger data for specified device
		bool getRangerInfo(RangerInfo * data, int deviceIndex = 0) const;
		/// get actual ranges
		const float * getRanges(int deviceIndex = 0) const;
		/// Set device position, relative to robot center
		void setRangerPose(float pose[3], int deviceIndex = 0);
		/// Read camera frame info
		bool getCameraInfo(CameraInfo * data, int deviceIndex = 0) const;
		/// Get camera data
		/// Frame data is copied to @data array
		bool getCameraData(unsigned char * output, int maxBytes = 0) const;
	protected:
		class Impl;
		/// Internal implementation instance
		Impl * impl;

		Client::Impl * getImpl();
	};

	enum ParamType
	{
		ParamTypeInvalid,
		ParamTypeInt,
		ParamTypeBool,
		ParamTypeFloat,
		ParamTypeString,
	};

	struct RNS_CLIENT_API Parameter
	{
		ParamType type;
		/// Actual parameter contents
		union
		{
			char stringData[255];
			int intData;
			float floatData;
			bool boolData;
		}data;

		Parameter();
		Parameter(const Parameter &param);
		Parameter & operator=(const Parameter & param);

		static Parameter fromString(const char * value);
		static Parameter fromInt(int value);
		static Parameter fromBool(bool value);
		static Parameter fromFloat(float value);
	};

	struct LogMessage
	{
		std::string message;
		std::string module;
		int level;
	};

	/*
			message TicketInfo
		{
			required int32 ticket = 1;			    ///< ticket ID
			required TicketResult state = 2;	  ///< Ticket state
		  optional string tag = 3;            ///< Additional tag
		  optional string type = 4;           ///< Ticket type (mover/player/servo)
		  optional string worker = 5;         ///< Worker id
		  optional TicketResult oldState = 6; ///< Old ticket state
		  optional string reason = 7;         ///< Reason for changing ticket state
		}
	 */

	struct TicketEvent
	{
		enum State
		{
			/// Result for ticket execution
			TicketNone = 0,			///< no changes for ticket
			TicketDone = 1,			///< ticket is done
			TicketFailed = 2,		///< ticket has failed
			TicketCanceled = 3,		///< ticket is cancelled
			TicketPostponed = 4,	///< ticket execution is postponed
			TicketActive = 5,   ///< Ticket is active right now
			TicketWait = 6,     ///< Ticket waits for execution
		};

		/// PUB-SUB id = PacketTicketState
		int id;
		std::string tag;
		std::string worker;
		std::string type;
		std::string reason;
		State state;
	};

	/// Implementation for interaction with SBrick system state
	class RNS_CLIENT_API SystemClient : public Client
	{
	public:
		/// Constructor
		SystemClient(Connector & connector);
		~SystemClient();

		/// request parameter list
		CommandResult requestParamList();

		///\brief Send to server updated parameter list
		/// Sends all local parameter to navigation server
		void sendParamList();

		///\brief Get single parameter value. Look into documentation to get available parameter list
		bool getParam(const char * name, Parameter & param);

		///\brief Set  single parameter
		/// Changes parameter value. Should call sendParamList to update parameters on the server
		bool setParam(const char * name, const Parameter & param);

		///\brief Send command to restart controller
		/// Sends command to reboot controller. It can take some time.
		bool restartRobot();

		///\brief Reads log message from internal queue
		///@returns true if message was obtained
		bool getLog(LogMessage & msg);

		///\ brief Reads ticket event from internal queue
		///@returns true if event was obtained
		bool getTicketEvent(TicketEvent & event);
	protected:
		class Impl;
		/// Internal implementation instance
		Impl * impl;
		Client::Impl * getImpl();
	};

	/// Implementation for direct control over robot chassis
	class RNS_CLIENT_API ChassisClient : public Client
	{
	public:
		/// Movement controller mode
		enum MoveMode
		{
			MoveDirect,			///< using direct movement commands
			MoveOdom,			///< move to point using odometry
			MoveNavigator,		///< move to global point using navigator (mapper+pathfinder)
		};

		///\brief Frame type for move orders
		/// Describes cpprdomate system, in which target coordinates are set
		enum TargetFrameType
		{
			FrameGlobal,		///< target coordinates are relative to global frame
			FrameRobotCurrent,	///< target coordinates are relative to robot current frame
			FrameLaser,			///< target coordinates are relative to robot laser frame
		};

		/// Constructor
		ChassisClient(Connector & connector);
		~ChassisClient();

		/// Set frame type for move commands
		void setTargetFrame(TargetFrameType mode);
		/// Get frame type for move commands
		TargetFrameType getTargetFrame() const;

		/// move to specific map coordinates
		CommandResult moveto(float x, float y, int timeout = 0);
		/// move to specific map coordinates asynchonically
		/// @returns task ticket number, or zero if task was rejected
		int asyncMoveto(float x, float y, int timeout = 0);

		/// Move to specific zone
		/// Starts asynchronous move task with zone as target
		/// @param zoneID - target zone id
		/// @param depth - distance to enter
		/// @returns task ticket number, or zero if task was rejected
		int asyncMoveto(int zoneId, double depth, int timeout = 0);

		/// Move to specific zone
    /// Starts asynchronous move task with zone as target
    /// @param zoneName - target zone name
    /// @param depth - distance to enter
    /// @returns task ticket number, or zero if task was rejected
    int asyncMoveto(const char * zoneName, double depth, int timeout = 0);

		/// Set current velocity to velx, vely, phi
		void drive(float velx, float vely, float phi);
		/// stop robot, removing all the tasks
		bool stop();
		/// get current velocity {x,y, angle} in robot local coordinates
		bool getVelocity(float velocity[3]) const;
		/// get current odometry {x,y, angle}
		bool getOdometry(float odometry[3]) const;
	protected:
		class Impl;
		/// Internal implementation instance
		Impl * impl;

		Client::Impl * getImpl();
	};

	/// Grid map information
	struct MapInfo
	{
		unsigned int width;		///< map width
		unsigned int height;	///< map height
		float resolution;		///< resolution. Pixel size in meters
		float pose[3];			///< position of map origin {x,y, angle}
	};

	/// Implementation for interaction with mapper
	class RNS_CLIENT_API NavigatorClient : public Client
	{
	public:
		/// Constructor
		NavigatorClient(Connector & connector);

		~NavigatorClient();

		/// set robot global pose
		void setRobotPose(float pose[3]);
		/// try to get full robot pose
		bool getRobotPose(float pose[3]) const;
		/// get map information
		bool getMapInfo(MapInfo & info) const;
		/// get actual map data
		/// @param data storage for map contents.
		/// Should be large enough to hold map contents.
		bool getOccupancy(signed char * data) const;
		/// get size for occupancy storage
		unsigned int getOccupancySize() const;
		///\brief Set map for robot
		/// Set map contents for navigation system. Robot enters UNKNOWN_POSITION state
		/// and should be told about current position, or global location search should be enabled
		void setMap(MapInfo & info, const void * data);
		/// Request map manually.
		CommandResult requestMap();
		///\brief Starts auto localization process
		///@returns true if request has been received, false otherwise
		bool startAutoLocalization();
		///\brief Enables map building mode
		///@returns true if request has been received, false otherwise
		bool enableMapBuilding();
		///\brief Disables map building mode
		///@returns true if request has been received, false otherwise
		bool disableMapBuilding();
		///\brief Asks server to clear map contents
		///@returns true if request has been received, false otherwise
		bool clearMap();
		///\brief Get robot confidence in its map location
		float getMapConfidence() const;
	protected:
		class Impl;
		/// Internal implementation instance
		Impl * impl;
		Client::Impl * getImpl();
	};

} // namespace RNS

#endif /* CLIENTS_H_ */
