///////////////////////////////////////////////////////////////////////////////////////
//
// IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
// By downloading, copying, installing or using the software you agree to this license.
// If you do not agree to this license, do not download, install,
// copy or use the software.
//
//
// License Agreement
// For RNS Client SDK
//
// Copyright (C) 2013-2014, Sybo Tech LLC, all rights reserved.
// Third party copyrights are property of their respective owners.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
// * Redistribution's of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// * Redistribution's in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
//
// * The name of the copyright holders may not be used to endorse or promote products
// derived from this software without specific prior written permission.
//
// This software is provided by the copyright holders and contributors "as is" and
// any express or implied warranties, including, but not limited to, the implied
// warranties of merchantability and fitness for a particular purpose are disclaimed.
// In no event shall the Intel Corporation or contributors be liable for any direct,
// indirect, incidental, special, exemplary, or consequential damages
// (including, but not limited to, procurement of substitute goods or services;
// loss of use, data, or profits; or business interruption) however caused
// and on any theory of liability, whether in contract, strict liability,
// or tort (including negligence or otherwise) arising in any way out of
// the use of this software, even if advised of the possibility of such damage.
//
//
/*
 * ZoneEditor.h
 *
 *  Created on: Apr 5, 2016
 *      Author: vrobot
 */

#ifndef INCLUDE_RNS_CLIENT_ZONEEDITOR_H_
#define INCLUDE_RNS_CLIENT_ZONEEDITOR_H_

#include "Clients.h"

#include <string>
#include <vector>

namespace RNS
{
	struct Point
	{
		float x;
		float y;
	};

	///\brief Zone
	/// Annotation to the map
	struct Zone
	{
		/// Unique ID
		int64_t id;

		enum ZoneType
		{
			ZonePoints,
			ZoneLines,
			ZonePoly,
		};

		ZoneType type;
		/// Zone name
		std::string name;
		/// Some additional information
		std::string description;
		/// Points that define zone boundary, specified in 'global' coordinates
		std::vector<Point> points;
		/// Some userdata. Could be a pointer or database index.
		int64_t userdataID;

		/// Zone size. Defines border 'thickness'
		float size;
		/// Flag shows if zone is restricted to enter
		bool restricted;
		/// Flag shows if there should be a notification when robot crosses this zone
		bool trigger;

		void clear();
	};

	///\brief ZoneEditor class
	/// Helper class to edit zone information on remote server
	class RNS_CLIENT_API ZoneEditor : public Client
	{
	public:
		ZoneEditor(Connector & connector);
		///\brief Requests zone info from server
		/// @returns request status
		CommandResult pull();
		///\brief Sends zone update to the server
		///@returns request status
		CommandResult push();
		///\brief Add new zone
		/// It is a local operation. Should call 'push' to send data to the server
		/// @returns pointer to the new zone
		Zone * addZone(Zone::ZoneType type);
		///\brief Read zone data
		/// @returns pointer to the new zone
		Zone * getZone(int id);
		///\brief Update zone contents
		/// It is a local operation. Should call 'push' to send data to the server
		int update(Zone * zone);
		///\brief Remove zone
		/// Removes zone locally
		/// @param id id to be removed
		int remove(int id);

		typedef int (*ZoneCallback)(Zone * zone, void * userdata);
		///@brief Get all local zones
		///@returns number of zones stored
		///@param zones array to store zone pointers. If NULL, function will just return number of zones
		/// This zones are 'volatile'. You should lock access to ZoneEditor to synchronize access from separate updater thread
		int getZones(ZoneCallback callback, void * userdata = NULL);
		///@brief Find zone by its name
		/// Function can be called several times
		///@param name zone name to be found
		///@returns zone ID. If id=0, no zone is found
		int findZone(const char * name, ZoneCallback callback, void * userdata = NULL);
	protected:
		Client::Impl * getImpl();
		struct Impl;
		Impl * impl;
	};
}// namespace RNS


#endif /* INCLUDE_RNS_CLIENT_ZONEEDITOR_H_ */
