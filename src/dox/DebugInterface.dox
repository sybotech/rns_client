/**
@page debug_interface Debug Interface

@section Overview

This interfaces serves as example of using RNS Client SDK to control a robot. 
It allows 
- View robot sensor data
- View image from camera (only from xtion-type sensors)
- View map and robot position on the map
- Control robot by screen joystick
- Send robot to point on the map
- View current task
 
RNS Interface is written in python and uses pyside as GUI. It also needs pyzmq and protobuf bindings. Interface can be obtained from rns_client [downloads area](https://bitbucket.org/sybotech/rns_client/downloads/rns_interface-1.0.zip)

Run @code python TCApp.py @endcode to start application  

@image html interface_disconnected.png

@section internal_parameters Internal parameters

Parameter list can be accessed by clicking "Configure server" button in connection pane. It works only if interface has successfully connected to SBrick server 

The following parameters are exposed to debug interface
Parameter name	|	Parameter description
----------------|----------------------------------
laser_pose/pose | laser rangefinder position, relative to robot center
xtion_pose/pose | xtion/kinect sensor position, relative to robot center
driver_laser/min_ang |	lower angular limit for laser rangefinder
driver_laser/max_ang |	upper angular limit for laser rangefinder
chassis/acceleration_linear |	linear acceleration for robot chassis
chassis/deceleration_linear	| linear deceleration for robot chassis
chassis/acceleration_angular |	angular acceleration for robot chassis
chassis/deceleration_angular |	angular deceleration for robot chassis
pathfinder/robotDiameter |		robot diameter for collision checking
pathfinder/clearDiameter |		obstacle size to be cleared away. Allows pathfinder to find path away of the obstacle, if collision has occurred somehow
pathfinder/ignoreDistance |		obstacles below this range are ignored. Should not exceed half of robot size
pathfinder/pathRetryPeriod |	specify time period between pathfinding attempts  
pathfinder/pathRetriesMax |		max attempts to find any path
mover/maxAngularVelocity |		angular velocity limit for automatic path following
mover/maxLinearVelocity |		linear velocity limit for automatic path following

*/