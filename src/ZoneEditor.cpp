/*
 * ZoneEditor.cpp
 *
 *  Created on: Apr 5, 2016
 *      Author: vrobot
 */


#include "internals.h"
#include "rns_client/ZoneEditor.h"

#include <set>
#include <vector>
#include <map>
#include <sstream>

namespace RNS
{

void Zone::clear()
{
	trigger = false;
	id = 0;
	restricted = false;
	userdataID = 0;
	size = 0.0;
	type = ZonePoly;
}

class ZoneEditor::Impl : public Client::Impl
{
public:
	/// Last assigned local id
	int64_t lastLocalID;
	/// Local zone revision
	int64_t localRevision;
	/// Server revision
	int64_t serverRevision;
	typedef int64_t id_t;
	/// Local zone storage
	typedef std::map<id_t, Zone*> Zones;

	Zones zones;

	/// List of locally updated IDs
	std::set<id_t> updated;
	/// List of locally deleted IDs
	std::set<id_t> deleted;
	/// New ids from the server. Local version does not present
	std::set<id_t> announced;

	/// IDs that were sent to server in AnnotationModify
	/// We need to keep this data to remap old ids to the new
	std::list<id_t> sent;

public:

	Impl()
	{
		clear();
	}

	void subscribe(Connector::Impl * impl)
	{
		Client::Impl::subscribe(impl);
		addSubscriber(impl, zmqTypes::PacketZoneUpdate, &Impl::onZoneUpdate, this);
	}

	virtual void reset()
	{
		clear();
		Client::Impl::reset();
	}

	/// Generate local ID
	int64_t genID()
	{
		--lastLocalID;
		if(lastLocalID >= 0)
		{
			printf("Zone ID overflow\n");
			lastLocalID = -1;
		}
		return lastLocalID;
	}

	void onZoneUpdate(const zmqTypes::AnnotationUpdate & msg)
	{
		DEBUG_LOG("ZoneEditor::onZoneUpdate()");
		zmqTypes::AnnotationAction action = msg.action();

		if(action == zmqTypes::AnnotationActionAdd)
		{
			/// TODO: implement
			/// Does server sends such updates?
		}

		if(action == zmqTypes::AnnotationActionNotify)
		{
			for(int i = 0; i < msg.new_ids_size(); i++)
			{
				int64_t id = msg.new_ids(i);
				/// TODO: What should we do with this set?
				this->announced.insert(id);
			}
		}

		if(action == zmqTypes::AnnotationActionDelete)
		{

			std::stringstream ss;
			for(int i = 0; i < msg.new_ids_size(); i++)
			{
				int64_t id = msg.new_ids(i);
				if(i == 0)
					ss << id;
				else
					ss << "," << id;

				if(zones.find(id) == zones.end())
				{
					/// There is no such zone for some reason
					ss << "!";
				}
				else
				{
					zones.erase(id);
				}
			}
			DEBUG_LOG("ZoneEditor::onZoneUpdate: server has deleted zones: {%s}", ss.str().c_str());
		}

		if(action == zmqTypes::AnnotationActionSet)
		{
			DEBUG_LOG("ZoneEditor::onZoneUpdate: server is going to replace all the database");
			/// TODO: Implement it
			/// Does server sends such update?
		}
	}

	/// Copy data from local structure to protobuf message
	void copy(const Zone & src, zmqTypes::Annotation & dst)
	{
		switch(src.type)
		{
		case Zone::ZonePoints:
			dst.set_type(zmqTypes::AnnotationTypePoint);
			break;
		case Zone::ZoneLines:
			dst.set_type(zmqTypes::AnnotationTypeLine);
			break;
		case Zone::ZonePoly:
			dst.set_type(zmqTypes::AnnotationTypePoly);
			break;
		default:
			DEBUG_LOG("ZoneEditor::copy Invalid zone type");
		}

		dst.set_id(src.id);
		dst.set_size(src.size);
		zmqTypes::Header * header = dst.mutable_header();
		header->set_frameid("map");
		header->set_timestamp(0);

		for(int i = 0; i < src.points.size(); i++)
		{
			zmqTypes::Vector3 * pt = dst.add_points();
			pt->set_x(src.points[i].x);
			pt->set_y(src.points[i].y);
			pt->set_z(0);
		}

		zmqTypes::AnnotationInfo & info = *dst.mutable_info();
		info.set_name(src.name);
		info.set_description(src.description);
		info.set_restricted(src.restricted);
		info.set_trigger(src.trigger);
	}

	/// Copy data from protobuf message to local structure
	void copy(const zmqTypes::Annotation & src, Zone & dst)
	{
		if(src.has_info())
		{
			const zmqTypes::AnnotationInfo & info = src.info();
			dst.name = info.name();
			dst.description = info.description();
			dst.restricted = info.restricted();
			dst.trigger = info.trigger();
			//dst.userdataID = info.u
		}

		dst.size = src.size();

		zmqTypes::AnnotationType type = src.type();
		if(type == zmqTypes::AnnotationTypePoint)
			dst.type = Zone::ZonePoints;
		if(type == zmqTypes::AnnotationTypeLine)
			dst.type = Zone::ZoneLines;
		if(type == zmqTypes::AnnotationTypePoly)
			dst.type = Zone::ZonePoly;

		dst.points.resize(src.points_size());

		for(int i = 0; i < src.points_size(); i++)
		{
			dst.points[i].x = src.points(i).x();
			dst.points[i].y = src.points(i).y();
		}

		dst.id = src.id();
	}

	void onResponseGetZones(const zmqTypes::AnnotationUpdate & msg)
	{
		this->serverRevision = msg.revision();

		std::set<id_t> updated;

		zmqTypes::AnnotationAction action = msg.action();
		/// TODO: figure out which action type is expected
		if(action == zmqTypes::AnnotationActionAdd)
		{
			for(int i = 0; i < msg.annotations_size(); i++)
			{
				const zmqTypes::Annotation & ann = msg.annotations(i);
				/// TODO: implement
			}
		}
	}

	Zone * getZone(id_t id)
	{
		auto it = zones.find(id);
		if(it != zones.end())
			return it->second;
		return NULL;
	}

	void onResponseModifyZones(const zmqTypes::AnnotationUpdate & msg)
	{
		if(msg.action() != zmqTypes::AnnotationActionNotify)
		{
			DEBUG_LOG("ZoneEditor::onResponseModifyZones() - wrong action type: %d", msg.action());
			return;
		}

		DEBUG_LOG("ZoneEditor::onResponseModifyZones() server revision=%d local revision=%d", msg.revision(), this->serverRevision);
		this->localRevision = this->serverRevision = msg.revision();

		std::list<id_t>::iterator it = this->sent.begin();
		for(int i = 0; i < msg.new_ids_size() && it != sent.end(); i++, it++)
		{
			id_t old_id = *it;
			id_t new_id = msg.new_ids(i);
			Zone * z = getZone(old_id);

			if(z == NULL)
			{
				DEBUG_LOG("ZoneEditor::onResponseModifyZones() - zone id=%d does not exist", old_id);
				continue;
			}

			if(getZone(new_id) != NULL)
			{
				DEBUG_LOG("ZoneEditor::onResponseModifyZones() - id=%d is occupied already", new_id);
				continue;
			}

			if(new_id == 0)
			{
				DEBUG_LOG("ZoneEditor::onResponseModifyZones() - server has removed zone id=%d", old_id);
				zones.erase(old_id);
			}
			else if(old_id != new_id)
			{
				DEBUG_LOG("ZoneEditor::onResponseModifyZones() - server has remapped zone id=%d->%d", old_id, new_id);
				z->id = new_id;
				zones.erase(old_id);
				zones.insert(std::make_pair(new_id, z));
			}
			else
			{
				DEBUG_LOG("ZoneEditor::onResponseModifyZones() - server has confirmed update for zone id=%d", new_id);
			}
		}
	}


	/// Create zone locally
	Zone * createLocal(Zone::ZoneType type)
	{
		Zone * z = new Zone();
		z->clear();
		z->id = genID();
		z->type = type;
		zones.insert(std::make_pair(z->id, z));
		return z;
	}

	/// clear local data
	void clear()
	{
		for(auto it: zones)
		{
			delete it.second;
		}
		zones.clear();
		updated.clear();
		announced.clear();
		sent.clear();

		localRevision = 0;
		serverRevision = 0;
		lastLocalID = 0;
	}
};


ZoneEditor::ZoneEditor(Connector& connector)
:Client(connector)
{
	impl = new Impl;
	assert(impl);
	connector.attach(this);
	impl->subscribe(connector.getImpl());
}


CommandResult ZoneEditor::pull()
{
	zmqTypes::AnnotationRequest request;
	request.set_full(true);
	request.set_needinfo(true);
	zmqTypes::AnnotationUpdate response;

	RequestResult rr = connector->getImpl()->sendMessage(this, zmqTypes::PacketRequestZones, &request, &response);
	if(rr == RequestHaveResponse)
	{
		impl->onResponseGetZones(response);
		return CommandDone;
	}
	return CommandFailed;
}

CommandResult ZoneEditor::push()
{
	zmqTypes::AnnotationUpdate request;
	//request.set_full(true);
	//request.set_needinfo(true);
	zmqTypes::AnnotationUpdate response;

	RequestResult rr = connector->getImpl()->sendMessage(this, zmqTypes::PacketModifyZones, &request, &response);
	if(rr == RequestHaveResponse)
	{
		impl->onResponseModifyZones(response);
		return CommandDone;
	}
	return CommandError;
}

Zone* ZoneEditor::addZone(Zone::ZoneType type)
{
	return impl->createLocal(type);
}

Zone* ZoneEditor::getZone(int id)
{
	Impl::Zones::iterator it = impl->zones.find(id);
	if(it == impl->zones.end())
		return NULL;
	return it->second;
}

int ZoneEditor::update(Zone* zone)
{
	if(zone != NULL)
	{
		if(zone->id != 0)
			impl->updated.insert(zone->id);
		else
		{

		}
	}
	return 0;
}

int ZoneEditor::remove(int id)
{
	Impl::Zones::iterator it = impl->zones.find(id);
	if(it == impl->zones.end())
		return 0;
	if(id > 0)
		this->impl->deleted.insert(id);
	else
	{
		impl->zones.erase(it);
		impl->updated.erase(id);
	}
	return 0;
}

int ZoneEditor::getZones(ZoneCallback callback, void * userdata)
{
	impl->lock();
	int counter = 0;
	try
	{
		for(auto it: impl->zones)
		{
			counter++;

			assert(it.second != nullptr);

			if(callback(it.second, userdata) <= 0)
				break;
		}
	}
	catch(...)
	{
		DEBUG_LOG("ZoneEditor::getZones() - unhandled exception");
	}
	impl->unlock();
	return counter;
}

int ZoneEditor::findZone(const char* name, ZoneCallback callback, void * userdata)
{
	impl->lock();
	int counter = 0;
	try
	{
		for(auto it: impl->zones)
		{
			counter++;

			assert(it.second != nullptr);

			if(it.second->name == name && callback(it.second, userdata) <= 0)
				break;
		}
	}
	catch(...)
	{
		DEBUG_LOG("ZoneEditor::findZone() - unhandled exception");
	}
	impl->unlock();
	return counter;
}

Client::Impl * ZoneEditor::getImpl()
{
	return impl;
}

}
