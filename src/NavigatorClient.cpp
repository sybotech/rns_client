/*
 * clients.cpp
 *
 *  Created on: May 6, 2014
 *      Author: vrobot
 */

#include "internals.h"
#include "RLE.h"

namespace RNS
{
	class NavigatorClient::Impl : public Client::Impl
	{
	public:
		float robotPose[3];
		bool hasMap;
		bool mapBuilding;
		MapInfo mapInfo;
		float confidence;
		std::vector<signed char> occupancy;

		Impl()
		{
			hasMap = false;
			mapBuilding = false;
			confidence = -1.0;
		}

		virtual void reset()
		{
			confidence = -1.0;
			hasMap = false;
			mapBuilding = false;

			Client::Impl::reset();
		}

		void subscribe(Connector::Impl * impl)
		{
			Client::Impl::subscribe(impl);
			addSubscriber(impl, zmqTypes::PacketMapOcc, &Impl::onMap, this);
			addSubscriber(impl, zmqTypes::PacketOdom, &Impl::onMapPose, this);
			addSubscriber(impl, zmqTypes::PacketState, &Impl::onRobotState, this);
		}

		void onMapPose(const zmqTypes::Odometry & odom)
		{
			Scalar yaw = 0, pitch = 0, roll = 0;
			getEuler(yaw, pitch, roll, &odom.pose().orientation());

			robotPose[0] = odom.pose().position().x();
			robotPose[1] = odom.pose().position().y();
			robotPose[2] = roll;
		}

		void onRobotState(const zmqTypes::State & state)
		{
			this->mapBuilding = state.mapisbuilding();
			// TODO: implement the rest
		}

		void onMap(const zmqTypes::OccupancyMap & message)
		{
			mapInfo.width = message.width();
			mapInfo.height = message.height();
			mapInfo.resolution = message.resolution();
			mapInfo.pose[0] = message.origin().position().x();
			mapInfo.pose[1] = message.origin().position().y();

			occupancy.resize(mapInfo.width*mapInfo.height);

			int contentsSize = message.map().size();

			if(message.compression() == zmqTypes::COMPRESSION_RLE)
			{
				RLE::decompress(mapInfo.width, mapInfo.height, message.map().c_str(), contentsSize, &occupancy.front());
			}
			else if(message.compression() == zmqTypes::COMPRESSION_RAW)
			{
				const char * data = message.map().c_str();
				occupancy.assign(data, data+contentsSize);
			}

			hasMap = true;
		}
	};

	NavigatorClient::NavigatorClient(Connector & connector)
		:Client(connector)
	{
		impl = new Impl();
		assert(impl != NULL);
		connector.attach(this);
		impl->subscribe(connector.getImpl());
	}

	NavigatorClient::~NavigatorClient()
	{
		impl->lock();
		detach();
		impl->unlock();
		delete impl;
		impl = NULL;
	}

	bool NavigatorClient::getMapInfo(MapInfo & info) const
	{
		if(!impl->hasMap)
			return false;
		impl->lock();
		memcpy(&info, &impl->mapInfo, sizeof(MapInfo));
		impl->unlock();
		return true;
	}

	bool NavigatorClient::getOccupancy(signed char * data) const
	{
		if(!impl->hasMap)
			return false;
		impl->lock();
		memcpy(data, &impl->occupancy.front(), impl->occupancy.size());
		impl->unlock();
		return true;
	}

	unsigned int NavigatorClient::getOccupancySize() const
	{
		unsigned int result = 0;
		impl->lock();
		result = impl->occupancy.front();
		impl->unlock();
		return result;
	}

	CommandResult NavigatorClient::requestMap()
	{
		zmqTypes::MapRequest request;
		//request.set_rotationalshit(false);
		request.set_compression(zmqTypes::COMPRESSION_RLE);
		zmqTypes::OccupancyMap response;
		RequestResult rr = connector->getImpl()->sendMessage(this, zmqTypes::PacketRequestMap, &request, &response);
		if(rr == RequestHaveResponse)
		{
			impl->onMap(response);
			return CommandDone;
		}
		return CommandFailed;
	}

	void setPose(zmqTypes::Pose & pose, float packedPose[3])
	{
		zmqTypes::Vector3 * position = pose.mutable_position();
		position->set_x(packedPose[0]);
		position->set_y(packedPose[1]);
		position->set_z(0);

		zmqTypes::Quaternion * orientation = pose.mutable_orientation();
		setEuler(0, 0, packedPose[2], orientation);
	}

	/// set robot global pose
	void NavigatorClient::setRobotPose(float robotPose[3])
	{
		zmqTypes::Goal request;

		request.mutable_header()->set_frameid("map");

		setPose(*request.mutable_goal(), robotPose);
		RequestResult rr = connector->getImpl()->sendMessage(this, zmqTypes::PacketRobotPose, &request);
	}

	void NavigatorClient::setMap(MapInfo & info, const void * data)
	{
		zmqTypes::OccupancyMap message;
		message.set_width(info.width);
		message.set_height(info.height);
		message.set_resolution(info.resolution);

		zmqTypes::Pose * pose = message.mutable_origin();
		setPose(*message.mutable_origin(), info.pose);
		std::vector<char> output;
		if(RLE::compress(info.width, info.height, (const signed char*)data, output))
		{
			message.set_compression(zmqTypes::COMPRESSION_RLE);
			message.set_map(&output.front(), output.size());
			connector->getImpl()->sendMessage(this, zmqTypes::PacketMapOcc, &message);
		}
	}

	/// try to get full robot pose
	bool NavigatorClient::getRobotPose(float pose[3]) const
	{
		if(impl->lastDataTick == 0)
			return false;
		memcpy(pose, impl->robotPose, sizeof(float)*3);
		return true;
	}

	/// Get robot confidence in its map location
	bool NavigatorClient::startAutoLocalization()
	{
		return impl->sendSimpleCmd(this, zmqTypes::CMD_MAP_AMCL_EXPLODE_HYPOTHESES)  != RequestError;
	}

	bool NavigatorClient::enableMapBuilding()
	{
		return impl->sendSimpleCmd(this, zmqTypes::CMD_MAP_BUILD) != RequestError;
	}

	bool NavigatorClient::disableMapBuilding()
	{
		return impl->sendSimpleCmd(this, zmqTypes::CMD_MAP_LOCATE) != RequestError;
	}

	bool NavigatorClient::clearMap()
	{
		return impl->sendSimpleCmd(this, zmqTypes::CMD_MAP_LOCATE) != RequestError;
	}

	float NavigatorClient::getMapConfidence() const
	{
		// TODO: Implement it
		return impl->confidence;
	}

	Client::Impl * NavigatorClient::getImpl()
	{
		return impl;
	}

}

