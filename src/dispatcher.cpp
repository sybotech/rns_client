/*
 * dispatcher.cpp
 *
 *  Created on: May 15, 2014
 *      Author: vrobot
 */

#include "MessageDispatcher.h"

MessageDispatcher::~MessageDispatcher()
{
	dropHandlers();
}

void MessageDispatcher::dropHandlers()
{
	for(Handlers::iterator it = handlers.begin(); it != handlers.end(); ++it)
		it->second->detach();
	handlers.clear();
}

MessageDispatcher::Subscriber MessageDispatcher::addHandler(const KeyType & key, HandlerBase * impl)
{
	Handlers::iterator it = handlers.find(key);
	MessageDispatcher::Subscriber sub(this, impl);
	handlers.insert(std::make_pair(key, impl));
	onSubscribe(key);
	return sub;
}

MessageDispatcher::HandlerBase * MessageDispatcher::findHandler(const KeyType & key)
{
	Handlers::iterator it = handlers.find(key);
	if(it != handlers.end())
		return it->second;
	return NULL;
}

void MessageDispatcher::removeHandler(MessageDispatcher::HandlerBase * handler)
{
	for(Handlers::iterator it = handlers.begin(); it != handlers.end(); ++it)
		if(it->second == handler)
		{
			handlers.erase(it);
			handler->detach();
			break;
		}
}

int MessageDispatcher::handleData(const KeyType & key, const void * data, unsigned int size)
{
	MessageDispatcher::HandlerBase * handler = findHandler(key);

	int handlersCalled = 0;

	for(Handlers::iterator it = handlers.lower_bound(key); it != handlers.upper_bound(key); ++it)
	{
		MessageDispatcher::HandlerBase * handler = it->second;
		if(handler != NULL)
		{
			if(handler->parse((char*)data, size))
			{
				handler->handle();
				handlersCalled++;
			}
			else
			{
				fprintf(stderr, "Failed to parse packet for key %d\n", (int)key);
			}
		}
	}
	return handlersCalled;
}

