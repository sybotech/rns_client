%module RNS;

%{
#include "rns_client/Clients.h"
#include "rns_client/ZoneEditor.h"
using namespace RNS;
%}

%nodefaultctor; // Disable constructor auto binding
%nodefaultdtor;	// Disable destructor auto binding 

///\brief RNS namespace
/// Contains all RNS-related classes

/// Some methods can fail by reason or timeout
enum CommandResult
{
	CommandDone,		///< Command has been executed
	CommandFailed,		///< Failed to execute command
	CommandTimeout,		///< Timeout has occurred
	CommandError,		///< Some sort of error has occurred
};

///\brief Wrapper around ZMQRoutines subscribe/publish framework.
/// It simplifies remote access to robot and provides basic methods, necessary for any client
class Client
{
public:
	virtual ~Client();
	/// Check if client is attached to connector
	/// @returns true if connection is valid
	bool valid() const;
	/// sleep for some time
	void sleep(int ms) const;
	/// wait until ... something happens
	CommandResult wait(int ms);
	/// check if new data is arrived during last update
	bool hasNewData() const;
	/// set max timeout for all blocking requests
	void setTimeout(int ms);
	/// Get assigned connector
	Connector * getConnector();
	/// Wait until ticket state changes, or timeout occurs
	CommandResult waitForTicket(int ticket, int ms = 0);
	/// Check if client is attached to any connector
	bool attached() const;
	/// detach itself from the connector
	void detach();
};

/// Deals all connection/ZMQ-related stuff
class Connector
{
public:
	Connector();
	Connector(const char * robotUrl);
	virtual ~Connector();

	/// Connect to robot controller
	/// @param robotUrl DNS name or IP addres of the controller
	/// @param subPort port for ZMQ SUB socket
	/// @param reqPort port for ZMQ REQ socket
	int connect(const char * robotUrl = "10.5.5.1", int subPort = 5557, int reqPort = 5559);
	/// if connection is established
	bool connected() const;
	/// wait until connection is established
	CommandResult waitForConnection(int timeMS = -1);
	/// Disconnect from controller
	void disconnect();

	class Impl;
	friend class Impl;

	Impl * getImpl();

	/// Detach a client
	void detach(Client * client);
	/// Attach a client
	bool attach(Client * client);
};

/// Information about range finder sensor
struct RangerInfo
{
	float rangeMin;
	float rangeMax;
	float angleMin;
	float angleMax;
	unsigned int beams;	/// number of beams
	float pose[3];		/// pose relative to robot kinematic center
};

/// Implementation for accessing ranger data
class SensorClient : public Client
{
public:
	/// Constructor
	SensorClient(Connector & connector);
	~SensorClient();
	/// Get number of available sensors
	int getRangersCount() const;
	/// read ranger data for specified device
	bool getRangerInfo(RangerInfo * data, int deviceIndex = 0) const;
	/// get actual ranges
	const float * getRanges(int deviceIndex = 0) const;
};

enum ParamType
{
	ParamTypeInvalid,
	ParamTypeInt,
	ParamTypeBool,
	ParamTypeFloat,
	ParamTypeString,
};

struct Parameter
{
	ParamType type;

	Parameter();
	Parameter(const Parameter &param);
	Parameter & operator=(const Parameter & param);

	static Parameter fromString(const char * value);
	static Parameter fromInt(int value);
	static Parameter fromBool(bool value);
	static Parameter fromFloat(float value);
};

/// Implementation for interaction with SBrick system state
class SystemClient : public Client
{
public:
	/// Constructor
	SystemClient(Connector & connector);
	~SystemClient();
	/// request parameter list
	CommandResult requestParamList();
	///\brief Send to server updated parameter list
	/// Sends all local parameter to navigation server
	void sendParamList();
	///\brief Get single parameter value. Look into documentation to get available parameter list
	bool getParam(const char * name, Parameter & param);
	///\brief Set  single parameter
	/// Changes parameter value. Should call sendParamList to update parameters on the server
	bool setParam(const char * name, const Parameter & param);

	///\brief Send command to restart controller
	/// Sends command to reboot controller. It can take some time.
	bool restartRobot();
};

/// Implementation for direct control over robot chassis
class ChassisClient : public Client
{
public:
	/// Movement controller mode
	enum MoveMode
	{
		MoveDirect,			///< using direct movement commands
		MoveOdom,			///< move to point using odometry
		MoveNavigator,		///< move to global point using navigator (mapper+pathfinder)
	};

	///\brief Frame type for move orders
	/// Describes cpprdomate system, in which target coordinates are set
	enum TargetFrameType
	{
		FrameGlobal,		///< target coordinates are relative to global frame
		FrameRobotCurrent,	///< target coordinates are relative to robot current frame
		FrameLaser,			///< target coordinates are relative to robot laser frame
	};

	/// Constructor
	ChassisClient(Connector & connector);
	~ChassisClient();

	void setTargetFrame(TargetFrameType mode);
	TargetFrameType getTargetFrame() const;

	/// move to specific map coordinates
	CommandResult moveto(float x, float y, int timeout = 0);
	/// Set current velocity to velx, vely, phi
	void drive(float velx, float vely, float phi);
	/// stop robot, removing all the tasks
	bool stop();
	/// get current velocity {x,y, angle} in robot local coordinates
	bool getVelocity(float velocity[3]) const;
	/// get current odometry {x,y, angle}
	bool getOdometry(float odometry[3]) const;
	
	/// move to specific map coordinates asynchonically
	/// @returns task ticket number, or zero if task was rejected
	int asyncMoveto(float x, float y, int timeout = 0);

	/// Move to specific zone
	/// Starts asynchronous move task with zone as target
	/// @param zoneID - target zone id
	/// @param depth - distance to enter
	/// @returns task ticket number, or zero if task was rejected
	int asyncMoveto(int zoneId, double depth, int timeout = 0);

	/// Move to specific zone
    /// Starts asynchronous move task with zone as target
    /// @param zoneName - target zone name
    /// @param depth - distance to enter
    /// @returns task ticket number, or zero if task was rejected
    int asyncMoveto(const char * zoneName, double depth, int timeout = 0);
};

/// Grid map information
struct MapInfo
{
	unsigned int width;		///< map width
	unsigned int height;	///< map height
	float resolution;		///< resolution. Pixel size in meters
	float pose[3];			///< position of map origin {x,y, angle}
};

%typemap(in, numinputs=0) float OUTPUT[3] (float tmp[3]){
	$1 = tmp;
}

%typemap(argout) float OUTPUT[3] {
      $result = PyTuple_New(3);
      PyTuple_SetItem($result, 0, PyFloat_FromDouble($1[0]));
      PyTuple_SetItem($result, 0, PyFloat_FromDouble($1[1]));
      PyTuple_SetItem($result, 0, PyFloat_FromDouble($1[2]));
}

/// Implementation for interaction with mapper
class NavigatorClient : public Client
{
public:
	/// Constructor
	NavigatorClient(Connector & connector);

	~NavigatorClient();
	/// set robot global pose
	void setRobotPose(float pose[3]);
	/// try to get full robot pose
	//bool getRobotPose(float pose[3]) const;
	%extend {
		void getRobotPose(float OUTPUT[3]) const
		{
			if (!$self->getRobotPose(OUTPUT))
			{
				OUTPUT[0] = 0.0f;
				OUTPUT[1] = 0.0f;
				OUTPUT[2] = 0.0f;
			}
		}
	}

	/// get map information
	bool getMapInfo(MapInfo & info) const;
	/// get actual map data
	/// @param data storage for map contents.
	/// Should be large enough to hold map contents.
	bool getOccupancy(signed char * data) const;
	/// get size for occupancy storage
	unsigned int getOccupancySize() const;
	///\brief Set map for robot
	/// Set map contents for navigation system. Robot enters UNKNOWN_POSITION state
	/// and should be told about current position, or global location search should be enabled
	void setMap(MapInfo & info, const void * data);
	/// Request map manually.
	CommandResult requestMap();
	///\brief Starts auto localization process
	///@returns true if request has been received, false otherwise
	bool startAutoLocalization();
	///\brief Enables map building mode
	///@returns true if request has been received, false otherwise
	bool enableMapBuilding();
	///\brief Disables map building mode
	///@returns true if request has been received, false otherwise
	bool disableMapBuilding();
	///\brief Asks server to clear map contents
	///@returns true if request has been received, false otherwise
	bool clearMap();
	///\brief Get robot confidence in its map location
	float getMapConfidence() const;
};

///\brief ZoneEditor class
/// Helper class to edit zone information on remote server
class ZoneEditor : public Client
{
public:
	ZoneEditor(Connector & connector);
	~ZoneEditor();
	///\brief Requests zone info from server
	/// @returns request status
	CommandResult pull();
	///\brief Sends zone update to the server
	///@returns request status
	CommandResult push();
	///\brief Add new zone
	/// It is a local operation. Should call 'push' to send data to the server
	/// @returns pointer to the new zone
	Zone * addZone(Zone::ZoneType type);
	///\brief Read zone data
	/// @returns pointer to the new zone
	Zone * getZone(int id);
	///\brief Update zone contents
	/// It is a local operation. Should call 'push' to send data to the server
	int update(Zone * zone);
	///\brief Remove zone
	/// Removes zone locally
	/// @param id id to be removed
	int remove(int id);

	typedef int (*ZoneCallback)(Zone * zone, void * userdata);
	///@brief Get all local zones
	///@returns number of zones stored
	///@param zones array to store zone pointers. If NULL, function will just return number of zones
	/// This zones are 'volatile'. You should lock access to ZoneEditor to synchronize access from separate updater thread
	//int getZones(ZoneCallback callback, void * userdata = NULL);
	///@brief Find zone by its name
	/// Function can be called several times
	///@param name zone name to be found
	///@returns zone ID. If id=0, no zone is found
	//int findZone(const char * name, ZoneCallback callback, void * userdata = NULL);
};