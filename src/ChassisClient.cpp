/*
 * ChassisClient.cpp
 *
 *  Created on: Jun 3, 2014
 *      Author: vrobot
 */


#include "internals.h"

#include <math.h>

namespace RNS
{

	std::string getFrameName(ChassisClient::TargetFrameType frame)
	{
		switch(frame)
		{
		case ChassisClient::FrameGlobal:
			return "map";
		case ChassisClient::FrameLaser:
			return "laser_link";
		case ChassisClient::FrameRobotCurrent:
			return "base_link";
		}
		return "map";
	}

	void setEuler(const Scalar& yaw, const Scalar& pitch, const Scalar& roll, zmqTypes::Quaternion * quat)
	{
		Scalar halfYaw = Scalar(yaw) * Scalar(0.5);
		Scalar halfPitch = Scalar(pitch) * Scalar(0.5);
		Scalar halfRoll = Scalar(roll) * Scalar(0.5);
		Scalar cosYaw = cos(halfYaw);
		Scalar sinYaw = sin(halfYaw);
		Scalar cosPitch = cos(halfPitch);
		Scalar sinPitch = sin(halfPitch);
		Scalar cosRoll = cos(halfRoll);
		Scalar sinRoll = sin(halfRoll);
		quat->set_x(cosRoll * sinPitch * cosYaw + sinRoll * cosPitch * sinYaw);
		quat->set_y(cosRoll * cosPitch * sinYaw - sinRoll * sinPitch * cosYaw);
		quat->set_z(sinRoll * cosPitch * cosYaw - cosRoll * sinPitch * sinYaw);
		quat->set_w(cosRoll * cosPitch * cosYaw + sinRoll * sinPitch * sinYaw);
	}

	void getEuler(Scalar& yaw, Scalar& pitch, Scalar& roll, const zmqTypes::Quaternion * quat)
	{
		Scalar x = quat->x();
		Scalar y = quat->y();
		Scalar z = quat->z();
		Scalar w = quat->w();

		yaw = (float)atan2(2.0*(y*z + w*x), w*w - x*x - y*y + z*z);
		pitch = (float)asin(-2.0*(x*z - w*y));
		roll = (float)atan2(2.0*(x*y + w*z), w*w + x*x - y*y - z*z);
	}

	/////////////////////////////////////////////////////////////////////////
	class ChassisClient::Impl : public Client::Impl
	{
	public:
		TargetFrameType frameType;

		Impl()
		{
			frameType = FrameGlobal;
			clear();
		}

		void subscribe(Connector::Impl * impl)
		{
			Client::Impl::subscribe(impl);
			addSubscriber(impl, zmqTypes::PacketOdom, &Impl::onOdomPose, this);
			addSubscriber(impl, zmqTypes::PacketGoal, &Impl::onGoalUpdate, this);
			addSubscriber(impl, zmqTypes::PacketState, &Impl::onRobotState, this);
		}

		virtual void reset()
		{
			clear();
			Client::Impl::reset();
		}

		void onRobotState(const zmqTypes::State & state)
		{
			// TODO: implement
		}

		void onOdomPose(const zmqTypes::Odometry & odom)
		{
			Scalar yaw = 0, pitch = 0, roll = 0;
			getEuler(yaw, pitch, roll, &odom.pose().orientation());

			lastOdometry[0] = odom.pose().position().x();
			lastOdometry[1] = odom.pose().position().y();
			lastOdometry[2] = roll;

			lastVelocity[0] = odom.twist().linear().x();
			lastVelocity[1] = odom.twist().linear().y();
			lastVelocity[2] = odom.twist().angular().z();
		}

		void onGoalUpdate(const zmqTypes::Goal & message)
		{
			Scalar yaw = 0, pitch = 0, roll = 0;
			const zmqTypes::Pose & target = message.goal();
			getEuler(yaw, pitch, roll, &target.orientation());
			goal[0] = target.position().x();
			goal[1] = target.position().y();
			goal[2] = roll;
		}

		float goal[3];	/// current goal from server
		float lastVelocity[3];
		float lastOdometry[3];
	protected:
		/// clear local data
		void clear()
		{
			lastOdometry[0] = 0;
			lastOdometry[1] = 0;
			lastOdometry[2] = 0;

			lastVelocity[0] = 0;
			lastVelocity[1] = 0;
			lastVelocity[2] = 0;
		}
	};

	ChassisClient::ChassisClient(Connector & connector)
	:Client(connector)
	{
		impl = new Impl;
		assert(impl);
		connector.attach(this);
		impl->subscribe(connector.getImpl());
	}

	ChassisClient::~ChassisClient()
	{
		DEBUG_LOG("~ChassisClient()-start");
		impl->lock();
		detach();
		impl->unlock();
		if(impl != NULL)
			delete impl;
		DEBUG_LOG("~ChassisClient()-done");
	}

	bool ChassisClient::stop()
	{
		return impl->sendSimpleCmd(this, zmqTypes::CMD_EMERG_DEACTIVATE)  != RequestError;
	}

	void ChassisClient::setTargetFrame(TargetFrameType mode)
	{
		impl->frameType = mode;
	}

	ChassisClient::TargetFrameType ChassisClient::getTargetFrame() const
	{
		return impl->frameType;
	}

	int ChassisClient::asyncMoveto(float x, float y, int timeout)
	{
		int resultTicket = 0;
		zmqTypes::Goal goal;

		goal.mutable_header()->set_frameid(getFrameName(impl->frameType));

		zmqTypes::Pose * pose = goal.mutable_goal();
		zmqTypes::Vector3 * position = pose->mutable_position();
		position->set_x(x);
		position->set_y(y);
		position->set_z(0);

		zmqTypes::Quaternion * orientation = pose->mutable_orientation();
		Scalar angle = 0.f;
		setEuler(0, 0, angle, orientation);

		if(connector != NULL)
		{
			zmqTypes::TicketInfo response;

			if(connector->getImpl()->sendMessage(this, zmqTypes::PacketGoal, &goal, &response) == RequestHaveResponse)
			{
				resultTicket = response.ticket();
				impl->storeTicket(resultTicket);
			}
		}
		return resultTicket;
	}

	int ChassisClient::asyncMoveto(int zoneId, double depth, int timeout)
  {
    int resultTicket = 0;
    zmqTypes::Goal goal;

    goal.set_zoneid(zoneId);

    if(connector != NULL)
    {
      zmqTypes::TicketInfo response;

      if(connector->getImpl()->sendMessage(this, zmqTypes::PacketGoal, &goal, &response) == RequestHaveResponse)
      {
        resultTicket = response.ticket();
        impl->storeTicket(resultTicket);
      }
    }
    return resultTicket;
  }

	int ChassisClient::asyncMoveto(const char * zoneName, double depth, int timeout)
  {
    int resultTicket = 0;
    zmqTypes::Goal goal;

    goal.set_zonename(zoneName);

    if(connector != NULL)
    {
      zmqTypes::TicketInfo response;

      if(connector->getImpl()->sendMessage(this, zmqTypes::PacketGoal, &goal, &response) == RequestHaveResponse)
      {
        resultTicket = response.ticket();
        impl->storeTicket(resultTicket);
      }
    }
    return resultTicket;
  }



	CommandResult ChassisClient::moveto(float x, float y, int timeout)
	{
		zmqTypes::Goal goal;

		int ticket = 0;

		goal.mutable_header()->set_frameid(getFrameName(impl->frameType));

		zmqTypes::Pose * pose = goal.mutable_goal();
		zmqTypes::Vector3 * position = pose->mutable_position();
		position->set_x(x);
		position->set_y(y);
		position->set_z(0);

		zmqTypes::Quaternion * orientation = pose->mutable_orientation();
		Scalar angle = 0.f;
		setEuler(0, 0, angle, orientation);

		if(connector != NULL)
		{
			zmqTypes::TicketInfo response;

			if(connector->getImpl()->sendMessage(this, zmqTypes::PacketGoal, &goal, &response) == RequestHaveResponse)
			{
				ticket = response.ticket();
				impl->storeTicket(ticket);
				DEBUG_LOG("ChassisClient::moveto(%f,%f) - got ticket %d", x,y, ticket);
			}
			else
			{
				DEBUG_LOG("ChassisClient::moveto(%f,%f) - no ticket obtained", x,y, ticket);
			}
		}
		if(ticket != 0)
		{
			return this->waitForTicketComplete(ticket, timeout);
		}

		return CommandFailed;
	}

	void ChassisClient::drive(float velx, float vely, float phi)
	{
		zmqTypes::Twist twist;

		zmqTypes::Vector3 * linear = twist.mutable_linear();
		linear->set_x(velx);
		linear->set_y(vely);
		linear->set_z(0);

		zmqTypes::Vector3 * angular = twist.mutable_angular();
		angular->set_x(0);
		angular->set_y(0);
		angular->set_z(phi);

		if(connector != NULL)
			connector->getImpl()->sendMessage(this, zmqTypes::PacketTwist, &twist);
	}

	bool ChassisClient::getVelocity(float velocity[3]) const
	{
		if(impl->lastDataTick == 0)
			return false;
		memcpy(velocity, impl->lastVelocity, sizeof(float)*3);
		return true;
	}

	bool ChassisClient::getOdometry(float odometry[3]) const
	{
		if(impl->lastDataTick == 0)
			return false;
		memcpy(odometry, impl->lastOdometry, sizeof(float)*3);
		return true;
	}

	Client::Impl * ChassisClient::getImpl()
	{
		return impl;
	}
}

