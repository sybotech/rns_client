/*
 * SystemClient.cpp
 *
 *  Created on: Jun 9, 2014
 *      Author: vrobot
 */

#include "internals.h"

namespace RNS
{
	Parameter::Parameter()
	{
		type = ParamTypeInvalid;
		memset(&data, 0, sizeof(data));
	}

	Parameter::Parameter(const Parameter &param)
	{
		type = param.type;
		memcpy(&data, &param.data, sizeof(data));
	}

	Parameter & Parameter::operator=(const Parameter & param)
	{
		memcpy(&data, &param.data, sizeof(data));
		return *this;
	}

	Parameter Parameter::fromString(const char * value)
	{
		Parameter result;
		result.type = ParamType::ParamTypeString;
		strcpy(result.data.stringData, value);
		return result;
	}

	Parameter Parameter::fromInt(int value)
	{
		Parameter result;
		result.type = ParamType::ParamTypeInt;
		result.data.intData = value;
		return result;
	}

	Parameter Parameter::fromBool(bool value)
	{
		Parameter result;
		result.type = ParamType::ParamTypeBool;
		result.data.boolData = value;
		return result;
	}

	Parameter Parameter::fromFloat(float value)
	{
		Parameter result;
		result.type = ParamType::ParamTypeFloat;
		result.data.floatData = value;
		return result;
	}

	class SystemClient::Impl : public Client::Impl
	{
	public:
		void onRobotState(const zmqTypes::State & state)
		{
			// TODO: implement
		}

		void onLog(const zmqTypes::LogUpdate & log)
		{
			// TODO: implement
			for(int i = 0; i < log.messages_size(); i++)
			{
				const zmqTypes::LogMessage & net_msg = log.messages(i);

				LogMessage msg;
				msg.message = net_msg.message();
				msg.module = net_msg.module();
				msg.level = net_msg.level();

				this->logCache.push_back(msg);
			}
		}

		void onTicketEvent(const zmqTypes::TicketInfo & ti)
		{
			TicketEvent evt;
			evt.id = ti.ticket();
			evt.state = static_cast<RNS::TicketEvent::State>(ti.state());
			if(ti.has_tag())
				evt.tag = ti.tag();
			if(ti.has_type())
				evt.type = ti.type();
			if(ti.has_reason())
				evt.reason = ti.reason();
			if(ti.has_worker())
				evt.worker = ti.worker();
			ticketEvents.push_back(evt);
		}

		void subscribe(Connector::Impl * impl)
		{
			Client::Impl::subscribe(impl);
			addSubscriber(impl,zmqTypes::PacketState, &Impl::onRobotState, this);
			addSubscriber(impl,zmqTypes::PacketLogMessages, &Impl::onLog, this);
			addSubscriber(impl,zmqTypes::PacketTicketState, &Impl::onTicketEvent, this);
		}

		void clear()
		{

		}

		virtual void reset()
		{
			clear();
			Client::Impl::reset();
		}

		std::list<LogMessage> logCache;
		std::list<TicketEvent> ticketEvents;

		void onParamList(const zmqTypes::ParameterList & paramList)
		{
			saveToCache(paramList, paramCache);
		}

		typedef std::map<std::string, zmqTypes::Parameter> CachedParameters;

		void saveToCache(const zmqTypes::ParameterList & paramList, CachedParameters & cache)
		{
			for(int i = 0; i < paramList.parameters_size(); i++)
			{
				const zmqTypes::Parameter & param = paramList.parameters(i);
				CachedParameters::iterator it = cache.find(param.name());

				if(it == cache.end())
				{
					zmqTypes::Parameter toSave = param;
					cache.insert(std::make_pair(param.name(), toSave));
				}
				else
					it->second.CopyFrom(param);

				printf("Got parameter %s\n", param.name().c_str());
			}
		}

		void loadFromCache(const CachedParameters & cache, zmqTypes::ParameterList & paramList)
		{
			for(auto it = cache.begin(); it != cache.end(); ++it)
			{
				zmqTypes::Parameter * p = paramList.add_parameters();
				p->CopyFrom(it->second);
			}
		}

		CachedParameters paramCache;

		zmqTypes::Parameter * findParam(const char * name)
		{
			CachedParameters::iterator it = paramCache.find(name);
			if(it != paramCache.end())
				return &it->second;
			return NULL;
		}

		bool getLog(LogMessage & msg)
		{
			bool result = false;
			this->lock();

			if(!logCache.empty())
			{
				msg = logCache.front();
				logCache.pop_front();
				result = true;
			}
			this->unlock();

			return result;
		}

		bool getTicketEvent(TicketEvent & event)
		{
			bool result = false;
			this->lock();

			if(!ticketEvents.empty())
			{
				event = ticketEvents.front();
				ticketEvents.pop_front();
				result = true;
			}
			this->unlock();

			return result;
		}
	};

	SystemClient::SystemClient(Connector & connector)
		:Client(connector)
	{
		impl = new Impl;
		assert(impl);
		connector.attach(this);
		impl->subscribe(connector.getImpl());
	}

	SystemClient::~SystemClient()
	{
		impl->lock();
		detach();
		impl->unlock();
		if(impl != NULL)
			delete impl;
	}

	CommandResult SystemClient::requestParamList()
	{
		zmqTypes::ParamRequest request;
		request.set_filter("");
		//request.set_compression(zmqTypes::COMPRESSION_JPEG);
		zmqTypes::ParameterList response;

		RequestResult rr = connector->getImpl()->sendMessage(this, zmqTypes::PacketRequestParamList, &request, &response);

		if(rr == RequestHaveResponse)
		{
			impl->onParamList(response);
			return CommandDone;
		}
		return CommandFailed;
	}

	/// send back updated parameter list
	void SystemClient::sendParamList()
	{
		zmqTypes::ParameterList parameters;
		impl->loadFromCache(impl->paramCache, parameters);
		connector->getImpl()->sendMessage(this, zmqTypes::PacketParamList, &parameters);
	}

	bool SystemClient::restartRobot()
	{
		return impl->sendSimpleCmd(this, zmqTypes::CMD_REBOOT_ROBOT) != RequestError;
	}

	bool SystemClient::getLog(LogMessage & msg)
	{
		return impl->getLog(msg);
	}

	bool SystemClient::getTicketEvent(TicketEvent & event)
	{
		return impl->getTicketEvent(event);
	}

	bool SystemClient::getParam(const char * name, Parameter & param)
	{
		// TODO: implement
		return false;
	}

	bool SystemClient::setParam(const char * name, const Parameter & param)
	{
		// TODO: implement
		return false;
	}

	/// Internal implementation instance
	Client::Impl * SystemClient::getImpl()
	{
		return impl;
	}

}
