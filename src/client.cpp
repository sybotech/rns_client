/*
 * client.cpp
 *
 *  Created on: May 15, 2014
 *      Author: vrobot
 */


#include "internals.h"
#include <stdarg.h>

namespace RNS
{
	void log_stdout(const char * format, ...)
	{
        va_list ap;
        va_start(ap,format);
        const int MAX_LEN=4096;
        static char buffer[MAX_LEN] = "";
#ifdef _MSC_VER
        vsprintf_s(buffer, MAX_LEN,format,ap);
#else
        vsnprintf(buffer, MAX_LEN,format,ap);
#endif
        buffer[MAX_LEN-1]=0;

        va_end(ap);
		fprintf(stderr, "%s\n", buffer);
	}

	void log_dummy(const char * fmt, ...)
	{

	}

	Client::Client(Connector & connector)
	{
		ticket = 0;
		this->connector = &connector;
	}

	Client::~Client()
	{
		DEBUG_LOG("Client::~Client()-start");
		detach();
		DEBUG_LOG("Client::~Client()-done");
	}

	void Client::detach()
	{
		if(connector != NULL)
		{
			this->connector->detach(this);
			connector = NULL;
		}
	}

	bool Client::attached() const
	{
		return connector != NULL;
	}

	Connector * Client::getConnector()
	{
		return connector;
	}

	void Client::reset()
	{
		getImpl()->reset();
	}

	void Client::newTick(unsigned int tick)
	{
		getImpl()->currentTick = tick;
	}

	void Client::sleep(int ms) const
	{
		DEBUG_LOG("Client::sleep(%d)", ms);
		std::chrono::milliseconds dura( ms );
		std::this_thread::sleep_for( dura );
	}

	CommandResult Client::waitForTicket(int ticket, int ms)
	{
		TicketInfo * info = getImpl()->getTicketInfo(ticket);
		if(info == NULL)
		{
			DEBUG_LOG("Client::waitForTicket(%d) - no ticket info", ticket);
			return CommandError;
		}
		DEBUG_LOG("Client::waitForTicket(%d) - waiting for ticket", ticket);
		return info->waitEvents(ms);
	}

	CommandResult Client::waitForTicketComplete(int ticket, int ms)
	{
		TicketInfo * info = getImpl()->getTicketInfo(ticket);
		if(info == NULL)
		{
			DEBUG_LOG("Client::waitForTicketComplete(%d) - no ticket info", ticket);
			return CommandError;
		}
		DEBUG_LOG("Client::waitForTicketComplete(%d) - waiting for ticket", ticket);
		return info->waitStateComplete(ms);
	}

	void Client::setTimeout(int ms)
	{
		// TODO: implement or remove it completely
	}

	bool Client::valid() const
	{
		return this->connector != NULL;
	}

	bool Client::hasNewData() const
	{
		/// Foul trickery
		Client * me = const_cast<Client*>(this);
		const Impl * impl = me->getImpl();
		return impl->hasNewData();
	}

	CommandResult Client::wait(int ms)
	{
		if(getImpl()->waitData(ms))
			return CommandDone;
		return CommandTimeout;
	}
	////////////////////////////////////////////////////////////////////
	Client::Impl::Impl()
	{
		processed = true;
		currentTick = 0;
		lastDataTick = 0;
		waiting = false;
	}

	Client::Impl::~Impl()
	{
		guard.lock();
		DEBUG_LOG("Client::~Impl()");
		dropTickets();
		guard.unlock();
	}

	void Client::Impl::subscribe(Connector::Impl * cimpl)
	{
		addSubscriber(cimpl, zmqTypes::PacketTicketState, &Impl::onTicketUpdate, this);
	}

	/// unsubscribe from network events
	void Client::Impl::unsubscribe()
	{
		subscribers.clear();
	}

	/// lock internal mutex
	void Client::Impl::lock()
	{
		guard.lock();
	}

	/// unlock internal mutex
	void Client::Impl::unlock()
	{
		guard.unlock();
	}

	/// notify that new data has arrived
	void Client::Impl::notifyData()
	{
		cond.notify_all();
	}

	bool Client::Impl::hasNewData() const
	{
		return lastDataTick > currentTick;
	}

	bool Client::Impl::waitData(int timeMS)
	{
		std::unique_lock<std::mutex> lk(guard);

		if(waiting == true)
		{
			DEBUG_LOG("Client::Impl::waitData() - already is waiting for something");
			return false;
		}
		waiting = true;

		if(timeMS < 0)
		{
			cond.wait(lk);
		}
		else
		{
			auto res = cond.wait_for(lk, std::chrono::milliseconds(timeMS));
		}
		waiting = false;
		return true;
	}

	std::string ticketStateToString(zmqTypes::TicketResult state)
	{
		switch(state)
		{
		case zmqTypes::TicketNone:
			return "None";
		case zmqTypes::TicketDone:
			return "Done";
		case zmqTypes::TicketCanceled:
			return "Canceled";
		case zmqTypes::TicketFailed:
			return "Failed";
		case zmqTypes::TicketPostponed:
			return "Postponed";
		case zmqTypes::TicketActive:
			return "Active";
		}
		char buffer[255];
		sprintf(buffer, "UnknownState:%d", state);
		return buffer;
	}

	void Client::Impl::onTicketUpdate(const zmqTypes::TicketInfo & message)
	{
		int ticket = message.ticket();
		zmqTypes::TicketResult state = message.state();
		TicketInfo * info = this->getTicketInfo(ticket);
		if(info != NULL)
		{
			DEBUG_LOG("Client::Impl::onTicketUpdate() ticket %d state has changed to %s", ticket, ticketStateToString(state).c_str());
			info->changeState(state);
		}
	}

	bool Client::Impl::storeTicket(int ticket)
	{
		if(tickets.find(ticket) != tickets.end())
		{
			DEBUG_LOG("Client::Impl::storeTicket(%d) was already stored", ticket);
			return false;
		}

		TicketInfo * ticketInfo = new TicketInfo;
		ticketInfo->id = ticket;
		ticketInfo->state = StateInit;
		ticketInfo->timeStart = -1;
		tickets[ticket] = ticketInfo;
		DEBUG_LOG("Client::Impl::storeTicket(%d) added new ticket", ticket);

		return true;
	}

	TicketInfo * Client::Impl::getTicketInfo(int ticket)
	{
		Tickets::iterator it = tickets.find(ticket);
		if(it == tickets.end())
			return NULL;
		return it->second;
	}

	void Client::Impl::dropTickets()
	{
		for(Tickets::iterator it = tickets.begin(); it != tickets.end(); ++it)
		{
			if(it->second != nullptr)
			{
				TicketInfo * info = it->second;
				info->ticketCmd = zmqTypes::TicketCanceled;
				info->cond.notify_all();
				info->guard.lock();
				info->guard.unlock();
				delete info;
			}
		}
	}
	/// reset internal data
	void Client::Impl::reset()
	{
		currentTick = 0;
		lastDataTick = 0;
		dropTickets();
	}

	RequestResult Client::Impl::sendSimpleCmd(Client * client, zmqTypes::COMMAND command)
	{
		zmqTypes::CommandList commands;
		commands.add_commandlist(command);
		Connector::Impl * impl = client->getConnector()->getImpl();
		return impl->sendMessage(client, zmqTypes::PacketSlot::PacketCommandList, &commands);
	}
/////////////////////////////////////////////////////////////////////////
	TicketInfo::TicketInfo()
	{
		this->id = 0;
		this->state = TicketState::StateInit;
		this->waiting = 0;
		this->timeStart = 0;
		this->ticketCmd = zmqTypes::TicketResult::TicketNone;
	}

	void TicketInfo::changeState(zmqTypes::TicketResult cmd)
	{
		if(cmd == zmqTypes::TicketResult::TicketNone)
			return;
		this->guard.lock();
		this->ticketCmd = cmd;
		this->guard.unlock();
		this->cond.notify_all();
	}

	CommandResult TicketInfo::waitEvents(int ms, zmqTypes::TicketResult * newState)
	{
		auto start = std::chrono::system_clock::now();
		auto time_finish = start + std::chrono::milliseconds(ms);

		while(true)
		{
			if(ms > 0 && std::chrono::system_clock::now() >=time_finish)
			{
				break;
			}
			std::unique_lock<std::mutex> lk(guard);
			waiting++;
			/// Is it safe for multiple thread-waiters?
			if(ms <= 0)
			{
				cond.wait(lk);
			}
			else
			{
				auto res = cond.wait_until(lk, time_finish);

				if(res == std::cv_status::timeout)
				{
					DEBUG_LOG("TicketInfo::waitEvents(%d) - got timeout waiting", this->id);
					break;
				}
				else
				{
					DEBUG_LOG("TicketInfo::waitEvents(%d) - got event %d", this->id, ticketCmd);
				}

			}

			zmqTypes::TicketResult cmd = ticketCmd;
			waiting--;

			/// We get here when we've got some event
			if(cmd != zmqTypes::TicketResult::TicketNone)
			{
				if(newState != NULL)
					*newState = cmd;
				return CommandDone;
			}
			break;
		}

		return CommandTimeout;
	}

	CommandResult TicketInfo::waitState(zmqTypes::TicketResult desired, int ms)
	{
		auto start = std::chrono::system_clock::now();
		auto time_finish = start + std::chrono::milliseconds(ms);

		zmqTypes::TicketResult cmd = zmqTypes::TicketResult::TicketNone;

		while(true)
		{
			if(ms > 0 && std::chrono::system_clock::now() >= time_finish)
			{
				break;
			}
			std::unique_lock<std::mutex> lk(guard);
			waiting++;
			/// TODO: need more checks for missed wakeup
			if(ms <= 0)
			{
				cond.wait(lk);
			}
			else
			{
				/// TODO: need more checks for timeouted wait
				auto res = cond.wait_until(lk, time_finish);
				if(res == std::cv_status::timeout)
				{
					DEBUG_LOG("TicketInfo::waitEvents(%d) - got timeout waiting", this->id);
					break;
				}
				DEBUG_LOG("TicketInfo::waitEvents() - interrupted");
			}

			zmqTypes::TicketResult cmd = ticketCmd;
			waiting--;

			if(ticketCmd == desired)
				return CommandDone;
		}

		return CommandTimeout;
	}

	CommandResult TicketInfo::waitStateComplete(int ms)
	{
		auto start = std::chrono::system_clock::now();
		auto time_finish = start + std::chrono::milliseconds(ms);

		zmqTypes::TicketResult cmd = zmqTypes::TicketResult::TicketNone;

		while(true)
		{
			if(ms > 0 && std::chrono::system_clock::now() >= time_finish)
			{
				break;
			}
			std::unique_lock<std::mutex> lk(guard);
			waiting++;
			/// TODO: need more checks for missed wakeup
			if(ms <= 0)
			{
				cond.wait(lk);
			}
			else
			{
				/// TODO: need more checks for timeouted wait
				auto res = cond.wait_until(lk, time_finish);
				if(res == std::cv_status::timeout)
				{
					DEBUG_LOG("TicketInfo::waitEvents(%d) - got timeout waiting", this->id);
					break;
				}
				DEBUG_LOG("TicketInfo::waitEvents() - interrupted");
			}

			zmqTypes::TicketResult cmd = ticketCmd;
			waiting--;

			if(ticketCmd == zmqTypes::TicketDone)
				return CommandDone;
			else if(ticketCmd == zmqTypes::TicketCanceled || ticketCmd == zmqTypes::TicketFailed)
				return CommandFailed;
		}

		return CommandTimeout;
	}
}
