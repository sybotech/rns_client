/*
 * SensorClient.cpp
 *
 *  Created on: Jun 3, 2014
 *      Author: vrobot
 */

#include "internals.h"
#include "nanojpeg.h"

namespace RNS
{
	class SensorClient::Impl : public Client::Impl
	{
	public:
		std::vector<RangerInfo> rangerInfo;
		std::vector<CameraInfo> cameraInfo;
		std::vector<float> ranges;

		bool hasCamera;

		struct ImageFrame
		{
			std::vector<unsigned char> data;
			int width;
			int height;
		};

		ImageFrame image;

		nj_context_t * nj;

		Impl()
		{
			hasCamera =  false;
			rangerInfo.resize(1);
			memset(&rangerInfo[0], 0, sizeof(RangerInfo));
			cameraInfo.resize(1);
			memset(&cameraInfo[0], 0, sizeof(CameraInfo));
			nj = njCreate();
		}

		~Impl()
		{
			njFree(nj);
		}

		void subscribe(Connector::Impl * impl)
		{
			Client::Impl::subscribe(impl);
			addSubscriber(impl,zmqTypes::PacketLaser, &Impl::onLaser, this);
			addSubscriber(impl,zmqTypes::PacketImage, &Impl::onCamera, this);
		}

		void onCamera(const zmqTypes::Image & message)
		{
			const char * data = message.image().c_str();
			int size = message.image().size();

			CameraInfo & ci = this->cameraInfo[0];

			int result = njDecode(nj, data, size);
			if(result == NJ_OK)
			{
				image.width = ::njGetWidth(nj);
				image.height = ::njGetHeight(nj);
				const unsigned char * decompressed_data = ::njGetImage(nj);
				int decompressed_size = ::njGetImageSize(nj);
				image.data.assign(decompressed_data, decompressed_data + decompressed_size);
				hasCamera = true;
				ci.width = image.width;
				ci.height = image.height;
				ci.data_size = decompressed_size;
				ci.bpp = ci.data_size / (ci.width * ci.height);
				if(::njIsColor(nj))
					strncpy(ci.fourcc, "rgb8", sizeof(ci.fourcc));
				else
					strncpy(ci.fourcc, "gray", sizeof(ci.fourcc));
			}
			else
			{
			}
			/*
			typedef enum _nj_result {
			    NJ_OK = 0,        // no error, decoding successful
			    NJ_NO_JPEG,       // not a JPEG file
			    NJ_UNSUPPORTED,   // unsupported format
			    NJ_OUT_OF_MEM,    // out of memory
			    NJ_INTERNAL_ERR,  // internal error
			    NJ_SYNTAX_ERROR,  // syntax error
			    __NJ_FINISHED,    // used internally, will never be reported
			} nj_result_t;
			*/
		}

		void onLaser(const zmqTypes::LaserScan & message)
		{
			RangerInfo & rf = this->rangerInfo[0];

			rf.rangeMin = message.rangemin();
			rf.rangeMax = message.rangemax();
			rf.angleMin = message.anglemin();
			rf.angleMax = message.anglemax();
			rf.beams = message.scan_size();

			ranges.resize(message.scan_size());

			for(int i = 0; i < message.scan_size(); i++)
				ranges[i] = message.scan(i);

			if(message.has_pose())
			{
				const zmqTypes::Pose & msgPose = message.pose();
				const zmqTypes::Vector3 & position = msgPose.position();

				Scalar yaw = 0, pitch = 0, roll = 0;
				getEuler(yaw, pitch, roll, &msgPose.orientation());

				rf.pose[0] = position.x();
				rf.pose[1] = position.y();
				rf.pose[2] = roll;
			}
		}
	};

	SensorClient::SensorClient(Connector & connector)
	:Client(connector)
	{
		impl = new Impl();
		assert(impl != NULL);
		connector.attach(this);
		impl->subscribe(connector.getImpl());
	}

	SensorClient::~SensorClient()
	{
		impl->lock();
		detach();
		impl->unlock();

		delete impl;
		impl = NULL;
	}

	int SensorClient::getRangersCount() const
	{
		return impl->rangerInfo.size();
	}

	bool SensorClient::getRangerInfo(RangerInfo * data, int deviceIndex) const
	{
		memcpy(data, &impl->rangerInfo[0], sizeof(RangerInfo));
		return true;
	}

	void SensorClient::setRangerPose(float pose[3], int deviceIndex)
	{
		zmqTypes::ParameterList paramList;
		zmqTypes::Parameter * param = paramList.add_parameters();
		char value[255] = "";

		if(deviceIndex == 0)
			param->set_name("/laser_pose/pose");
		else if(deviceIndex == 1)
			param->set_name("/xtion_pose/pose");
		else
		{
			sprintf(value, "/laser_pose%d/pose", deviceIndex);
			param->set_name(value);
		}

		param->set_type(zmqTypes::ParamType::ParamString);
		sprintf(value, "%f %f 0 0 0 %f", pose[0], pose[1], pose[2]);
		param->set_strvalue(value);

		connector->getImpl()->sendMessage(this, zmqTypes::PacketParamList, &paramList);
	}

	bool SensorClient::getCameraInfo(CameraInfo * info, int deviceIndex) const
	{
		if(!impl->hasCamera)
			return false;
		impl->lock();
		memcpy(info, &impl->cameraInfo[deviceIndex], sizeof(CameraInfo));
		impl->unlock();
		return true;
	}

	bool SensorClient::getCameraData(unsigned char * output, int maxBytes) const
	{
		if(!impl->hasCamera)
			return false;
		impl->lock();

		int frame_size = impl->image.data.size();

		if(maxBytes <= 0)
			maxBytes = frame_size;

		int frame_bytes = std::min<int>(maxBytes, frame_size);
		memcpy(output, &impl->image.data.front(), frame_bytes);
		impl->unlock();
		return true;
	}

	const float * SensorClient::getRanges(int deviceIndex) const
	{
		if(impl->ranges.empty())
			return NULL;
		return &impl->ranges.front();
	}

	Client::Impl * SensorClient::getImpl()
	{
		return impl;
	}
}
