/*
 * connector.cpp
 *
 *  Created on: May 15, 2014
 *      Author: vrobot
 */

#include "internals.h"

namespace RNS
{
	Connector::Connector()
	{
		impl = new Impl(this);
	}

	Connector::Connector(const char * path)
	{
		impl = new Impl(this);
		connect(path);
	}

	Connector::~Connector()
	{
		DEBUG_LOG("~Connector() - start");
		detachAll();
		disconnect();
		if(impl != NULL)
			delete impl;
		DEBUG_LOG("~Connector() - end");
	}

	Connector::Impl * Connector::getImpl()
	{
		return impl;
	}

	// subPort = 5557, repPort = 5559
	int Connector::connect(const char * robotUrl, int subPort, int repPort, int pushPort)
	{
		return impl->connect(robotUrl, subPort, repPort, pushPort);
	}

	void Connector::detach(Client * client)
	{
		impl->lock();
		for(auto it = impl->clients.begin(); it != impl->clients.end(); ++it)
		{
			Client * c = *it;
			if(client == c)
			{
				impl->clients.erase(it);
				break;
			}
		}
		impl->unlock();
	}

	bool Connector::attach(Client * client)
	{
		std::unique_lock<std::mutex> lk(impl->guard);
		if(!impl->hasClient(client))
		{
			DEBUG_LOG("Connector::attach()");
			impl->clients.push_back(client);
			return true;
		}
		else
		{
			DEBUG_LOG("Connector::attach() tried to attach existing client");
		}
		return false;
	}

	void Connector::detachAll()
	{
		// TODO: implement. All clients should be unsubscribed here
	}

	CommandResult Connector::waitForConnection(int time)
	{
		std::unique_lock<std::mutex> lk(impl->guard);

		if(!impl->valid())
			return CommandError;

		if(impl->cond.wait_for(lk, std::chrono::milliseconds(time)) == std::cv_status::no_timeout)
			return CommandDone;

		return CommandTimeout;
	}

	bool Connector::connected() const
	{
		return impl != NULL ? impl->connected() : false;
	}

	void Connector::disconnect()
	{
		DEBUG_LOG("Connector::disconnect()-start");

		impl->disconnect();

		for(std::list<Client*>::iterator it = impl->clients.begin(); it != impl->clients.end(); ++it)
		{
			Client * c = *it;
			c->reset();
		}
		DEBUG_LOG("Connector::disconnect()-end");
	}

	//////////////////////////////////////////////////////////////////////
	Connector::Impl::Impl(Connector * connector)
	{
		reqPort = 0;
		subPort = 0;
		requesterState = RequesterInvalid;
		updateTick = 0;
		bytesReceived = 0;
		bytesSent = 0;
		dataTick = 0;
		context = zmq_ctx_new();
		assert(context != NULL);
		storageBuffer.resize(0xfffff);
	}

	Connector::Impl::~Impl()
	{
		DEBUG_LOG("Connector::Impl::~Impl() - start");
		if(connected())
			disconnect();

		DEBUG_LOG("Connector::Impl::~Impl() - destroying ZMQ context");
		if (context)
		{
			zmq_ctx_destroy(context);
			context = NULL;
		}
		DEBUG_LOG("Connector::Impl::~Impl() - done");
	}

	bool Connector::Impl::valid() const
	{
		std::lock_guard<std::mutex> lock(guard);
		return requester.valid() && subscriber.valid() && pusher.valid();
	}

	bool Connector::Impl::connected() const
	{
		if(!valid())
			return false;
		std::lock_guard<std::mutex> lock(guard);
/*
		if(bytesReceived == 0)
			return false;
*/
		const unsigned int ticksToDisconnect = 10;

		if(updateTick - dataTick > ticksToDisconnect)
			return false;

		return true;
	}

	void Connector::Impl::disconnect()
	{
		DEBUG_LOG("Connector::Impl::disconnect() - start");

		if(!requester.valid())
		{
			DEBUG_LOG("Connector::Impl::disconnect() - requester is already NULL");
		}
		if(!subscriber.valid())
		{
			DEBUG_LOG("Connector::Impl::disconnect() - subscriber is already NULL");
		}
		if(!pusher.valid())
		{
			DEBUG_LOG("Connector::Impl::disconnect() - pusher is already NULL");
		}

//		assert(requester != NULL && subscriber != NULL);

		if(thread.joinable())
		{
			DEBUG_LOG("Connector::Impl::disconnect() - stopping spinner thread");
			guard.lock();
			shouldExit = true;
			guard.unlock();
			cond.notify_one();
			thread.join();
			DEBUG_LOG("Connector::Impl::disconnect() - spinner thread has stopped");
		}

		requester.close();
		subscriber.close();
		pusher.close();

		updateTick = 0;
		dataTick = 0;
		bytesSent = 0;
		bytesReceived = 0;

		DEBUG_LOG("Connector::Impl::disconnect() - done");
	}

	bool Connector::Impl::read(int timeoutMS)
	{
		if(!valid())
			return false;

		updateTick++;

		this->lock();
		std::list<Client*> tempClients = clients;
		for(auto it = tempClients.begin(); it != tempClients.end(); ++it)
		{
			Client * c = *it;
			c->newTick(updateTick);
		}
		this->unlock();

		/// Read messages here
		return receive(timeoutMS);
	}

	void Connector::Impl::spinThread(Impl * impl)
	{
		DEBUG_LOG("Connector::Impl::spinThread()-start");
		while(true)
		{
			bool shouldExit = false;
			impl->lock();
			shouldExit = impl->shouldExit;
			impl->unlock();

			if(shouldExit)
				break;

			const int readDelayMS = 20;

			impl->read(readDelayMS);
			std::chrono::milliseconds dura( readDelayMS );
			std::this_thread::sleep_for( dura );
		}
		DEBUG_LOG("Connector::Impl::spinThread()-exited");
	}

	int Connector::Impl::connect(const char * robotUrl, int subPort, int reqPort, int pushPort)
	{
		DEBUG_LOG("Connector::Impl::connect(%s;sub=%d;req=%d;push=%d)-start", robotUrl, subPort, reqPort, pushPort);

		if(connected())
			disconnect();

		std::lock_guard<std::mutex> lock(guard);
		this->url = robotUrl;
		this->subPort = subPort;
		this->reqPort = reqPort;
		char address[256] = "";
		int result = 0;

		/// Connecting subscriber
		subscriber.init(context, ZMQ_SUB);
		sprintf(address, "tcp://%s:%d", robotUrl, subPort);
		address[255] = 0;
		DEBUG_LOG("Connector::Impl::connect() connecting subscriber to %s", address);
		result = zmq_connect(subscriber, address);

		if(result != 0)
		{
			DEBUG_LOG("Connector::Impl::connect() : Failed to init subscriber: %s", strerror(errno));
			disconnect();
			return -1;
		}

		/// Connecting requester
		requester.init(context, ZMQ_REQ);
		sprintf(address, "tcp://%s:%d", robotUrl, reqPort);
		address[255] = 0;
		DEBUG_LOG("Connector::Impl::connect() connecting requester to %s", address);
		result = zmq_connect(requester, address);
		if(result != 0)
		{
			DEBUG_LOG("Connector::Impl::connect() : Failed to init requester: %s", strerror(errno));
			disconnect();
			return -1;
		}

		/// Connecting pusher
		pusher.init(context, ZMQ_PUSH);
		sprintf(address, "tcp://%s:%d", robotUrl, pushPort);
		address[255] = 0;
		DEBUG_LOG("Connector::Impl::connect() connecting pusher to %s", address);
		result = zmq_connect(pusher, address);

		if(result != 0)
		{
			DEBUG_LOG("Connector::Impl::connect() : Failed to init requester: %s", strerror(errno));
			disconnect();
			return -1;
		}

		int linger = 1000;
		zmq_setsockopt(requester,ZMQ_LINGER, &linger, sizeof(linger));

		requesterState = RequesterState::RequesterIdle;

		shouldExit = false;
		// Creating spinner thread
		thread = std::thread(spinThread, this);

		DEBUG_LOG("Connector::Impl::connect()-end");
		return 0;
	}

	int Connector::Impl::wait(int timeMS)
	{
		// TODO: implement
		return 0;
	}

	void Connector::Impl::onSubscribe(const KeyType & key)
	{
		DEBUG_LOG("Connector::Impl::onSubscribe(%d)", (int)key);
		char header = key;
		std::lock_guard<std::mutex> lock(guard);
		zmq_setsockopt(subscriber, ZMQ_SUBSCRIBE, &header, sizeof(header));
	}

	void Connector::Impl::onUnsubscribe(const KeyType & key)
	{
		DEBUG_LOG("Connector::Impl::onUnsibscribe(%d)", (int)key);
		char header = key;
		std::lock_guard<std::mutex> lock(guard);
		zmq_setsockopt(subscriber, ZMQ_UNSUBSCRIBE, &header, sizeof(header));
	}

	int Connector::Impl::pushMessage(Client * client, zmqTypes::PacketSlot slot, MessagePtr * message)
	{
		DEBUG_LOG("Connector::Impl::pushMessage(%d, %s)", slot, message->GetTypeName().c_str());

		Connector * connector = client->getConnector();
		if(connector == 0)
			return RequestError;

		if(message != NULL)
		{
			int size = message->ByteSize();
			if(storageBuffer.size() < size)
				storageBuffer.resize(size);

			char * dataBuffer = &storageBuffer.front();

			if(!message->SerializeToArray(dataBuffer+1, storageBuffer.size() - 1))
			{
				DEBUG_LOG("Failed to serialize message %s\n", message->GetTypeName().c_str());
				DEBUG_LOG("Current message contents = %s\n", message->DebugString().c_str());
			}
			dataBuffer[0] = slot;

			zmq_send(pusher, dataBuffer, size+1, 0);
		}
		else
		{
			char header = slot;
			zmq_send(pusher, &header, 1, 0);
		}

		return 0;
	}

	RequestResult Connector::Impl::sendMessage(Client * client, zmqTypes::PacketSlot slot, MessagePtr * message, MessagePtr * response)
	{
		//char dataBuffer[0xfffff];
		DEBUG_LOG("Connector::Impl::sendMessage(%s)", message->GetTypeName().c_str());

		Connector * connector = client->getConnector();
		if(connector == 0)
			return RequestError;

		RequestResult result = RequestError;

		if(message != NULL)
		{
			int size = message->ByteSize();
			if(storageBuffer.size() < size)
				storageBuffer.resize(size);

			char * dataBuffer = &storageBuffer.front();

			if(!message->SerializeToArray(dataBuffer+1, storageBuffer.size() - 1))
			{
				DEBUG_LOG("Failed to serialize message %s\n", message->GetTypeName().c_str());
				DEBUG_LOG("Current message contents = %s\n", message->DebugString().c_str());
			}
			dataBuffer[0] = slot;

			zmq_send(requester, dataBuffer, size+1, 0);
		}
		else
		{
			char header = slot;
			zmq_send(requester, &header, 1, 0);
		}

		requesterState = RequesterState::RequesterWaitingResponse;

		// Get response here
		{
			zmq_msg_t responseContainer;
			zmq_msg_init(&responseContainer);

			zmq_pollitem_t poller;
			poller.socket = requester;
			poller.events = ZMQ_POLLIN;
			poller.fd = 0;

			const int timeout = 1000;	//milliseconds
			int rc = zmq_poll(&poller, 1, timeout);

			if(rc > 0)
			{
				int size = zmq_msg_recv(&responseContainer, requester, 0);
				char * dataPtr = NULL;

				if(response == NULL)
					result = RequestDone;
				//else if (size > 0 && response->ParseFromArray((char*)zmq_msg_data(&responseContainer), size))
				else if (size > 0 && response->ParseFromString(std::string((char*)zmq_msg_data(&responseContainer)+1, size-1)))
				{
					printf("Got response with %d bytes\n", size);
					result = RequestHaveResponse;
				}
				else
					result = RequestError;

				zmq_msg_close(&responseContainer);
			}
		}
		return result;
	}

	bool Connector::Impl::receive(int timeoutMS)
	{
		bool result = true;
		zmq_msg_t msg;
		zmq_msg_init(&msg);

		zmq_pollitem_t pi;
		pi.socket = subscriber;
		pi.events = ZMQ_POLLIN;

		if(zmq_poll(&pi, 1, timeoutMS*1000) <= 0)
			return false;

		int size = zmq_msg_recv(&msg, subscriber, 0);

		char * dataPtr = NULL;

		if (size > 0)
		{
			dataPtr = (char*)zmq_msg_data(&msg);
			bytesReceived += size;
		}
		else
			result = false;

		if(dataPtr != NULL)
		{
			/*
			if(size > 0)
				DEBUG_LOG("Got packet: %d\n", (int)dataPtr[0]);*/
			dataTick = updateTick;
			char key = dataPtr[0];
			this->lock();
			for(Handlers::iterator it = handlers.lower_bound(key); it != handlers.upper_bound(key); ++it)
			{
				MessageDispatcher::HandlerBase * handler = it->second;
				Client::Impl::ClientPtr * clientPtr = dynamic_cast<Client::Impl::ClientPtr*>(handler);
				if(clientPtr != NULL)
				{
					Client::Impl * cimpl = clientPtr->client;
					bool parsed = false;

					cimpl->lastDataTick = updateTick;
					if(!handler->parse(dataPtr+1, size-1))
					{
						DEBUG_LOG("Failed to parse packet for key %d\n", (int)key);
					}
					else
						parsed = true;

					if(parsed && !handler->handle())
					{
						DEBUG_LOG("Failed to invoke handler for key %d\n", (int)key);
					}
					else
					{
						cimpl->notifyData();
					}
				}
			}
			this->unlock();
		}

		zmq_msg_close(&msg);

		return result;
	}

	bool Connector::Impl::hasClient(Client * client) const
	{
		for(auto it = clients.begin(); it != clients.end(); ++it)
		{
			Client * c = *it;
			if(client == c)
				return true;
		}
		return false;
	}

	void Connector::Impl::lock()
	{
		guard.lock();
	}

	void Connector::Impl::unlock()
	{
		guard.unlock();
	}
}
