/*
 * RLE.h
 *
 *  Created on: Jun 12, 2014
 *      Author: vrobot
 */

#ifndef RLE_H_
#define RLE_H_

#include <vector>

/// Contains routines for RLE coding and decoding of ROS occupancy grid
namespace RLE
{
	/// Bits count for row length
	const int RLE_LengthBits = 6;
	const int RLE_LengthMax = (1<<RLE_LengthBits) - 1;

	union RLE_Item
	{
		struct
		{
			unsigned int value : 2;
			unsigned int length : RLE_LengthBits;
		}packed;
		char data;
	};

	const int occupancyWall = 80;

	/// pack ROS occupancy to 2bit representation
	inline unsigned int packOccupancy(int value)
	{
		if(value < 0)
			return 2;
		if(value < occupancyWall)
			return 0;
		return 1;
	}

	/// unpack from 2bit representation to ROS occupancy
	inline unsigned int unpackOccupancy(int value)
	{
		if(value == 2)
			return -50;
		if(value == 0)
			return 0;
		return occupancyWall;
	}

	/// does compression stuff. Result is stored in @param data
	inline bool compress(unsigned int width, unsigned int height, const signed char * source, std::vector<char> & data)
	{
		if(width == 0 || height == 0)
			return false;

		data.resize(0);
		data.reserve(width*height);

		unsigned int lastValue = packOccupancy(source[0]);
		unsigned int length = 0;
		unsigned int totalLength = 0;

		for(size_t i = 0; i < width*height; i++)
		{
			unsigned int newValue = packOccupancy(source[i]);
			length++;

			if(newValue != lastValue || length == RLE_LengthMax)
			{
				RLE_Item item;
				item.packed.value = lastValue;
				item.packed.length = length;
				data.push_back(item.data);
				totalLength += length;
				length = 0;
			}
			lastValue = newValue;
		}

		RLE_Item item;
		item.packed.value = lastValue;
		item.packed.length = length;

		totalLength += length;
		data.push_back(item.data);

		return true;
	}

	/// decompress from RLE to ROS occupancy
	inline bool decompress(unsigned int width, unsigned int height, const char * source, unsigned int records, signed char * result)
	{
		if(width == 0 || height == 0)
			return false;

		unsigned int targetPos = 0;

		for(unsigned int i = 0; i < records; i++)
		{
			RLE_Item item;
			item.data = source[i];
			char occupancy = unpackOccupancy(item.packed.value);

			for(unsigned int length = item.packed.length; length > 0; length--)
			{
				result[targetPos] = occupancy;

				targetPos++;
				if(targetPos == width*height)
					break;
			}
			if(targetPos == width*height)
				break;
		}
		return true;
	}
}

#endif /* RLE_H_ */
