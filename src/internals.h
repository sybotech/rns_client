/*
 * internals.h
 *
 *  Created on: May 15, 2014
 *      Author: vrobot
 */

#ifndef INTERNALS_H_
#define INTERNALS_H_

#include <list>
#include <set>
#include "messages.pb.h"

#include "MessageDispatcher.h"
#include <mutex>
#include <condition_variable>
#include <thread>
#include <zmq.h>

#include "rns_client/Clients.h"

#if defined(  _DEBUG) || !defined( NDEBUG)
#define DEBUG_LOG log_stdout
#else
#define DEBUG_LOG log_dummy
#endif

namespace RNS
{
	typedef ::google::protobuf::Message MessagePtr;
	void log_stdout(const char * fmt, ...);
	void log_dummy(const char * fmt, ...);
	/// Result for request
	enum RequestResult
	{
		RequestError = -1,
		RequestDone = 0,
		RequestHaveResponse = 1,
	};

	/// State for REQ socket
	enum RequesterState
	{
		RequesterInvalid,			/// Before connection has occured
		RequesterIdle,				/// can serve requests
		RequesterWaitingResponse,	/// already serving request
	};


	class Client;

	/// Implements most IO used by connector
	class Connector::Impl : public MessageDispatcher
	{
	public:
		std::string url;
		int subPort;
		int reqPort;
		void * context;			/// ZMQ context

		volatile bool shouldExit;
		unsigned int updateTick;
		unsigned int dataTick;

		unsigned int bytesReceived;
		unsigned int bytesSent;

		class ZMQError
		{

		};

		class ZMQSocket
		{
			void * sock;

		public:
			ZMQSocket()
			{
				sock = NULL;
			}
			~ZMQSocket()
			{
				close();
			}

			void init(void * context, int type)
			{
				close();
				sock = zmq_socket(context, type);
				if(sock == NULL)
				{
					throw ZMQError();
				}
			}

			bool valid() const
			{
				return sock != NULL;
			}

			void close()
			{
				if(sock != NULL)
				{
					zmq_close(sock);
					sock = NULL;
				}
			}

			operator void*()
			{
				return sock;
			}
		};

		ZMQSocket requester;		/// ZMQ publisher socket
		ZMQSocket subscriber;		/// ZMQ subscriber socket
		ZMQSocket pusher;			/// ZMQ push socket


		Impl(Connector * connector);
		~Impl();

		bool hasClient(Client * client) const;

		static void spinThread(Impl * impl);

		void lock();
		void unlock();

		/// Wait for any incoming data
		int wait(int timeoutMS);

		bool valid() const;

		bool receive(int timeoutMS);
		/// connect to specific port
		int connect(const char * robotUrl, int subPort, int repPort, int pushPort);
		/// check if connection is valid
		bool connected() const;
		/// disconnect from remote host
		void disconnect();
		/// wait until task ticket is executed or timeout occurs
		CommandResult waitForTicket(int ticket, int timeMS = -1);

		bool read(int timeoutMS);

		void onSubscribe(const KeyType & key);
		void onUnsubscribe(const KeyType & key);

		/// sends message to server and gets confirmation reply
		RequestResult sendMessage(Client * client, zmqTypes::PacketSlot slot, MessagePtr * message, MessagePtr * response = NULL);
		int pushMessage(Client * client, zmqTypes::PacketSlot slot, MessagePtr * message);

		mutable std::mutex guard;
		std::condition_variable cond;
		std::thread thread;
		std::vector<char> storageBuffer;

		std::list<Client*> clients;

		RequesterState requesterState;
	};


	enum TicketState
	{
		StateInit,
		StateSuspended,
		StateActive,
		StateDone,
		StateFailed,
		StateCanceled,
	};

	/// Task ticket info
	struct TicketInfo
	{
		unsigned int id;
		unsigned int timeStart;
		TicketState state;
		zmqTypes::TicketResult ticketCmd;
		std::mutex guard;
		std::condition_variable cond;
		int waiting;

		TicketInfo();

		void changeState(zmqTypes::TicketResult ticketCmd);
		/// Wait for any state change
		/// @param newState storage for new state value
		CommandResult waitEvents(int ms, zmqTypes::TicketResult * newState = NULL);
		/// Wait for desired ticket state
		CommandResult waitState(zmqTypes::TicketResult desired, int ms);
		/// Wait for desired ticket state
		CommandResult waitStateComplete(int ms);
		/// Check if there were new events
		bool hasEvents() const;
	};
	/// Client implementation
	class Client::Impl
	{
	public:
		std::list<MessageDispatcher::Subscriber> subscribers;	/// store subscribers here to safely unsibscribe at class destructo

		friend class Connector::Impl;

		class ClientPtr
		{
		public:
			Client::Impl * client;
			ClientPtr(Client::Impl * client) : client(client) {}
		};

		/// Father, please forgive me for using multiple inheritance
		template<class Target, class Message>
		class ClientMethodHandler : public ProtoTraits::MethodHandler<Target, Message>, public ClientPtr
		{
		public:
			typedef void (Target::*Signature) (const Message & msg);

			ClientMethodHandler(Target * target, Signature callback)
			:ProtoTraits::MethodHandler<Target, Message>(target, callback), ClientPtr(target)
			{}
		};

		unsigned int currentTick;		///< current update tick
		unsigned int lastDataTick;		///< current tick with incoming data

		std::condition_variable cond;
		std::mutex guard;
		bool waiting;

		bool processed;					///< flag shows that connector works with this client right now

		typedef std::map<int, TicketInfo * > Tickets;
		Tickets tickets;

		TicketInfo * getTicketInfo(int ticket);

		Impl();

		virtual ~Impl();

		template <class Target, class Message> void addSubscriber(Connector::Impl * cimpl, int key, void (Target::*method)(const Message & message), Target *target)
		{
			subscribers.push_back(cimpl->addHandler(key, new Client::Impl::ClientMethodHandler<Target, Message>(target, method)));
		}

		virtual void subscribe(Connector::Impl * cimpl);
		/// unsubscribe from network events
		virtual void unsubscribe();
		/// lock internal mutex
		void lock();
		/// unlock internal mutex
		void unlock();
		/// notify that new data has arrived
		void notifyData();
		bool waitData(int timeMS);
		void onTicketUpdate(const zmqTypes::TicketInfo & message);

		bool hasNewData() const;

		/// Drops all tracked tickets
		void dropTickets();
		/// Used to track new ticket
		bool storeTicket(int ticket);
		/// Sends simple command
		RequestResult sendSimpleCmd(Client * client, zmqTypes::COMMAND command);
		/// reset internal data
		virtual void reset();
	};

	typedef float Scalar;
	void setEuler(const Scalar& yaw, const Scalar& pitch, const Scalar& roll, zmqTypes::Quaternion * quat);
	void getEuler(Scalar& yaw, Scalar& pitch, Scalar& roll, const zmqTypes::Quaternion * quat);
}

#endif /* INTERNALS_H_ */
