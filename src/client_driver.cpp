

#include <time.h>

#include <zmq.h>
#include <string>
#include <iostream>
#ifndef _WIN32
#include <unistd.h>
#else
#include <windows.h>
#endif

#include <chrono>
#include <iostream>

#include "driver_data.pb.h"

#include "rns_client/ClientDriver.h"

namespace RNS {

	using std::chrono::nanoseconds;
	using std::chrono::milliseconds;
	using std::chrono::duration_cast;

	//using namespace driver_data;
	//using namespace std;
	using std::cout;
	using std::string;

	typedef driver_data::Odometry ProtobufOdometry;
	typedef driver_data::Joystick ProtobufJoystick;

	typedef std::chrono::high_resolution_clock Clock;
	typedef std::chrono::time_point<Clock> Time;

	/// RobotDriverClient Impl implementation
	class RobotDriverClient::Impl {

		void * context;
		void * socket;

		Time lastSuccessfullSend;
		Time lastSuccessfullReceive;
		Joystick lastJoystic;
		Odometry lastOdometry;
		bool lastSendWasSuccessfull;
		bool lastReceiveWasSuccessfull;

		std::string address;
		int socketWaitTimeMilisec;
		int deadTimeMilisec;
		int reconnectTimeMilisec;

	public:

		///RobotDriverClient Impl constructor
		Impl (const char * newConnectAddr, int newSocketWaitTimeMilisec, int newDeadTimeMilisec, int newReconnectTimeMilisec)
		{
			//Impl (std::string newConnectAddr, int newSocketWaitTimeMilisec = 100, int newDeadTimeMilisec = 500, int newReconnectTimeMilisec = 5000): context(1) {
			address = newConnectAddr;
			socketWaitTimeMilisec = newSocketWaitTimeMilisec;
			deadTimeMilisec = newDeadTimeMilisec;
			reconnectTimeMilisec = newReconnectTimeMilisec;

			lastJoystic.x_speed = 0.0;
			lastJoystic.y_speed = 0.0;
			lastJoystic.angle_speed = 0.0;
			context = zmq_ctx_new();

			connect();
		}

		/// open connection
		void connect()
		{
			socket = zmq_socket(context, ZMQ_PAIR);
			zmq_setsockopt(socket, ZMQ_SNDTIMEO, &socketWaitTimeMilisec, sizeof(socketWaitTimeMilisec));
			zmq_setsockopt(socket, ZMQ_RCVTIMEO, &socketWaitTimeMilisec, sizeof(socketWaitTimeMilisec));
			zmq_connect(socket, address.c_str());

			lastSuccessfullReceive = Clock::now();
			lastSuccessfullReceive = Clock::now();
			lastSendWasSuccessfull = true;
			lastReceiveWasSuccessfull = true;
		}

		/// reconntect manually since ZMQ_PAIR doesn't reconnect by itself
		void reconnect() {
			zmq_close(socket);
			socket = NULL;
			connect();
		}

		/// receve joystic command
		int receive(Joystick &j)
		{
			zmq_msg_t message;
			zmq_msg_init(&message);

			zmq_pollitem_t poller;
			poller.socket = socket;
			poller.events = ZMQ_POLLIN;
			poller.fd = 0;

			const int timeout = 1000;	//milliseconds
			int rc = zmq_poll(&poller, 1, timeout);

			if(rc > 0)
			{
				int size = zmq_msg_recv(&message, socket, 0);
				const char * data = (const char*)zmq_msg_data(&message);

				lastSuccessfullReceive = Clock::now();
				lastReceiveWasSuccessfull = true;

				if (data[0] == driver_data::JoystickByte)
				{
					ProtobufJoystick joy;
					joy.ParseFromArray(data+1, size - 1);
					j.x_speed = joy.x_speed();
					j.y_speed = joy.y_speed();
					j.angle_speed = joy.angle_speed();

					return 1;
				} else {
					// Heartbeat
					return 1;
				}

			} else {
				Time time_now = Clock::now();
				//int milisec_diff = duration_cast<milliseconds>(time_now - lastSuccessfullSend).count();
				int milisec_diff = (int)duration_cast<milliseconds>(time_now - lastSuccessfullReceive).count();
				lastReceiveWasSuccessfull = false;

				j = lastJoystic;

				if (milisec_diff > reconnectTimeMilisec) {
					reconnect();
					return 0;
				}

				if (milisec_diff > deadTimeMilisec) {
					j.x_speed = 0;
					j.y_speed = 0;
					j.angle_speed = 0;
					return 1;
				}
				return 0;
			}

			/*
			catch(zmq::error_t &error)
			{
//				printf("Exception handled: %s\n", error.what());
			}*/
			return 0;
		}


		/// try to resend last odometry if last send was not successfull
		int resend()
		{
			/*
			if (!lastSendWasSuccessfull)
			{
				return send(lastOdometry);
			}*/
			return 1;
		}

		//int send(zmq::message_t & msg)
		int send(const std::vector<char> &buffer)
		{
			int result = 0;
			int bytes = zmq_send(socket, &buffer.front(), buffer.size(), 0);
			//if ( socket->send(msg) > 0 )
			if(bytes > 0)
			{
				lastSuccessfullSend = Clock::now();
				lastSendWasSuccessfull = true;
				result = 1;
			} else
			{
				Time time_now = Clock::now();
				int milisec_diff = (int)duration_cast<milliseconds>(time_now - lastSuccessfullSend).count();
				lastSendWasSuccessfull = false;

				if (milisec_diff > reconnectTimeMilisec)
				{
					// reconnect manually
					reconnect();
				}
			}
			return result;
		}

		int send(const LaserInfo & info, double * ranges)
		{
			if(info.beams <= 0)
				return 0;

			driver_data::LaserScan message;

			message.set_anglemin(info.angle_min);
			message.set_anglemax(info.angle_max);
			message.set_rangemin(info.range_min);
			message.set_rangemax(info.range_max);
			message.set_angleinc((info.angle_max - info.angle_min) / (double)info.beams);

			// TODO: write proper timestamp
			message.set_stamp(0);
			for(int i = 0; i < info.beams; i++)
				message.add_scan(ranges[i]);
			// Serialize message
			int msgSize = message.ByteSize();

			std::vector<char> buffer(msgSize);
			char *p = &buffer.front();
			//zmq::message_t serializedMessage(msgSize);
			//char *p = (char *) serializedMessage.data();
			p[0] = driver_data::LaserScanByte;
			message.SerializeToArray(&(p[1]), msgSize);
			// Send it
			return send(buffer);
		}

		/// send odometry data to server
		int send(const Odometry &newTOdom) {
			lastOdometry = newTOdom;

			ProtobufOdometry newOdom;
			newOdom.set_x((float)newTOdom.x);
			newOdom.set_y((float)newTOdom.y);
			newOdom.set_angle((float)newTOdom.angle);
			//newOdom.set_tmp(5);
			//lastOdometry = newOdom;

			int msgSize = newOdom.ByteSize();

			int result = 0;

			//zmq::message_t newOdomMsg(msgSize);
			//char *p = (char *) newOdomMsg.data();
			std::vector<char> buffer(msgSize+1);
			char *p = &buffer.front();

			p[0] = driver_data::OdometryByte;
			//memcpy( (void *) &(p[1]), , 5);
			newOdom.SerializeToArray(p+1, msgSize);

			return send(buffer);
		}
	};


	RobotDriverClient::RobotDriverClient(const char *newConnectAddr, int newSocketWaitTimeMilisec, int newDeadTimeMilisec, int newReconnectTimeMilisec)
		: pi(new Impl(newConnectAddr, newSocketWaitTimeMilisec, newDeadTimeMilisec, newReconnectTimeMilisec))
	{

	}

	void RobotDriverClient::connectSocket() {
		pi->connect();
	}

	void RobotDriverClient::reconnectSocket() {
		pi->reconnect();
	}

	int RobotDriverClient::receive(Joystick &j) {
		return pi->receive(j);
	}

	int RobotDriverClient::resend() {
		return pi->resend();;
	}

	int RobotDriverClient::send(const Odometry &newOdom) {
		return pi->send(newOdom);
	}

	int RobotDriverClient::send(const LaserInfo & info, double * ranges)
	{
		return pi->send(info, ranges);
	}

}
