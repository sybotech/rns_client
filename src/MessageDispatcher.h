/*
 * MessageDispatcher.h
 *
 *  Created on: May 15, 2014
 *      Author: vrobot
 */

#ifndef MESSAGEDISPATCHER_H_
#define MESSAGEDISPATCHER_H_

#include <map>
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>	// for fprintf
#include <string>

/// Class for holding message dispatcher map.
class MessageDispatcher
{
public:
	/// Base virtual class for handling incoming packets
	class HandlerBase
	{
		mutable int refs;
		bool attached;
	public:
		HandlerBase()
		{
			refs = 0;
			attached = true;
		}

		virtual ~HandlerBase() {}

		void detach()
		{
			attached = false;
		}

		bool isAttached() const
		{
			return attached;
		}

		int addRef() const // mutable
		{
			return ++refs;
		}

		int decRef() const // mutable
		{
			return --refs;
		}
		/// Parse incoming data. It should be stored internally
		virtual bool parse(const char * data, unsigned int size) = 0;
		/// does all data parsing. Returns false if parsing failed
		virtual bool handle() const = 0;
	};

	typedef int KeyType;

    /// Handler for ordinary functions/static methods. No actual data is supposed
	class SimpleFunctionHandler : public HandlerBase
	{
	public:
		typedef void (*Signature) ();
		Signature callback;
		SimpleFunctionHandler(Signature callback) : callback(callback) {}

		virtual bool parse(const char * data, unsigned int size)
		{
			return size == 0;
		}

		virtual bool handle() const
		{
			callback();
			return true;
		}
	};

	/// Handler for ordinary functions/static methods. No actual data is supposed
	template<class Target> class SimpleMethodHandler : public HandlerBase
	{
	public:
		typedef Target target_class;
		typedef void ( Target::*Signature) ();
		Signature callback;
		mutable Target * target;

		SimpleMethodHandler(Target * target, Signature callback) : callback(callback), target(target) {}

		virtual bool parse(const char * data, unsigned int size)
		{
			return size == 0;
		}

		virtual bool handle() const
		{
			(target->*callback)();
			return true;
		}
	};

	/// Handler for ordinary const methods. No actual data is supposed
	template<class Target> class SimpleMethodHandlerC : public HandlerBase
	{
	public:
		typedef void (Target::*Signature) () const;
		Signature callback;
		const Target * target;

		SimpleMethodHandlerC(const Target * target, Signature callback) : callback(callback), target(target) {}

		virtual bool parse(const char * data, unsigned int size)
		{
			return size == 0;
		}

		virtual bool handle() const
		{
			(target->*callback)();
			return true;
		}
	};

	class Subscriber
	{
		HandlerBase * handler;
		MessageDispatcher * dispatcher;

		void release()
		{
			if(handler != NULL)
			{
				int ref = handler->decRef();
				if(ref == 0)
				{
					if(handler->isAttached())
						dispatcher->removeHandler(handler);

					delete handler;
				}
			}
		}
	public:
		Subscriber()
		{
			handler = NULL;
			dispatcher = NULL;
		}

		Subscriber(MessageDispatcher * dispatcher, HandlerBase * handler)
		{
			handler->addRef();
			this->handler = handler;
			this->dispatcher = dispatcher;
		}

		Subscriber(const Subscriber & sub)
		{
			if(sub.handler != NULL)
			{
				sub.handler->addRef();
				handler = sub.handler;
			}
			this->dispatcher = sub.dispatcher;
		}

		~Subscriber()
		{
			release();
		}

		Subscriber & operator=(const Subscriber & sub)
		{
			if(sub.handler != handler)
			{
				if(sub.handler != NULL)
					sub.handler->addRef();
				release();
				handler = sub.handler;
			}
			return *this;
		}

		// get stored pointer
		HandlerBase * ptr()
		{
			return handler;
		}
	};

	/// Add subscriber to dispatcher map
	/// All stored subscribers will be removed in the destructor
	Subscriber addHandler(const KeyType & key, HandlerBase * handler);
	HandlerBase * findHandler(const KeyType & key);

	/// delete all subscriptions
	void dropHandlers();

	virtual ~MessageDispatcher();

	void removeHandler(HandlerBase * handler);

	int handleData(const KeyType & key, const void * data, unsigned int size);
protected:
	// called when subscribed to new key
	virtual void onSubscribe(const KeyType & key) = 0;
	// called when no keys are subscribed to
	virtual void onUnsubscribe(const KeyType & key) = 0;

	typedef std::multimap<KeyType, HandlerBase*> Handlers;
	Handlers handlers;
};

/// Contains handlers for protobuf messages
namespace ProtoTraits
{
	/// Base handler class to parse protobuf messages
    template<class Message> class Handler : public MessageDispatcher::HandlerBase
    {
    protected:
    	Message msg;
    public:
    	/// parses message and calls appropriate callback
    	virtual bool parse(const char * data, unsigned int size)
    	{
    		//std::string(data, size)
    		//if(!msg.ParseFromArray(data, size))
    		if(!msg.ParseFromString(std::string(data, size)))
    		{
    			fprintf(stderr, "Failed to parse data using %s\n", msg.GetTypeName().c_str());
    			return false;
    		}
    		return true;
    	}
    };

    /// Handler for ordinary functions/static methods
    template<class Message> class FunctionHandler : public Handler<Message>
    {
    public:
    	typedef void (*Signature) (const Message & msg );
    	Signature callback;
    	FunctionHandler(Signature callback) : callback(callback) {}
    	virtual ~FunctionHandler() {}

    	virtual bool handle() const
    	{
    		if(callback == NULL)
    			return false;
    		callback(this->msg);
    		return true;
    	}
    };

    /// Handler for ordinary functions/static methods
	template<class Target, class Message> class MethodHandler : public Handler<Message>
	{
	public:
		typedef void (Target::*Signature) (const Message & msg );
		Signature callback;
		Target * target;
		MethodHandler(Target * target, Signature callback) : callback(callback), target(target){}
		virtual ~MethodHandler() {}

		virtual bool handle() const
		{
			if(target == NULL)
				return false;
			(target->*callback)(this->msg);
			return true;
		}
	};

	/// Handler for const class methods
	template<class Target, class Message> class MethodHandlerC : public Handler<Message>
	{
	public:
		typedef void (Target::*Signature) (const Message & msg ) const;
		Signature callback;
		const Target * target;
		MethodHandlerC(Target * target, Signature callback) : callback(callback), target(target){}
		virtual ~MethodHandlerC(){}

		virtual bool handle() const
		{
			if(target == NULL)
				return false;
			(target->*callback)(this->msg);
			return true;
		}
	};
};

#endif /* MESSAGEDISPATCHER_H_ */
