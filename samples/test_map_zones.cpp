/** \example test_map_zones.cpp
 *
 *  Created on: Jun 9, 2014
 *
 *      Author: Dmitry Kargin
 *      Example shows how to get map and zone info and draw it
 */

#include <rns_client/Clients.h>
#include <rns_client/ZoneEditor.h>
#include <opencv2/opencv.hpp>
#include <stdio.h>
#include <functional>



/// Converts from world coordinates to pixel coordinates
void convert2map(const RNS::MapInfo &mf, float from[2], float to[2])
{
	to[0] = (from[0] - mf.pose[0])/mf.resolution;
	to[1] = (from[1] - mf.pose[1])/mf.resolution;
}

/// Add two 2d orientations, specified by [x; y; angle] triplet
void sum_poses(float pose_a[3], float pose_b[3], float result[3])
{
	float cs = cos(pose_a[2]);
	float sn = sin(pose_a[2]);

	result[0] = pose_a[0] + cs * pose_b[0] - sn * pose_b[1];
	result[1] = pose_a[1] + sn * pose_b[0] + cs * pose_b[1];
	result[2] = pose_a[2] + pose_b[2];
}

void drawZonePoints(const RNS::MapInfo & mf, const RNS::Zone * zone, cv::Mat & image, cv::Scalar color)
{

}

void drawZoneLines(const RNS::MapInfo & mf, const RNS::Zone * zone, cv::Mat & image, cv::Scalar color)
{

}

void drawZonePoly(const RNS::MapInfo & mf, const RNS::Zone * zone, cv::Mat & image, cv::Scalar color)
{
	std::vector<cv::Point> points;

	for(int i = 0; i < zone->points.size(); i++)
	{
		float map[2];
		float world[2] = {zone->points[i].x,zone->points[i].y};
		convert2map(mf, world, map);
		points.push_back(cv::Point(map[0], map[1]));
	}

	const cv::Point * ptr = &points.front();
	int npts = points.size();
	cv::fillPoly(image, &ptr, &npts, 1, color);
}

/// Draws zone to image
void drawZone(const RNS::MapInfo & mf, const RNS::Zone * zone, cv::Mat & image, cv::Scalar color)
{
	switch(zone->type)
	{
	case RNS::Zone::ZonePoints:
		drawZonePoints(mf, zone, image, color);
		break;
	case RNS::Zone::ZoneLines:
		drawZoneLines(mf, zone, image, color);
		break;
	case RNS::Zone::ZonePoly:
		drawZonePoly(mf, zone, image, color);
		break;
	}
}

using namespace cv;

int main(int argc, char * argv[])
{
	/// specify DNS name or IP address
	const char * robotAddress = argc > 1 ? argv[1] : "localhost";

	RNS::Connector connector(robotAddress);
	RNS::NavigatorClient navigator(connector);
	RNS::SystemClient sysClient(connector);
	RNS::ZoneEditor editor(connector);

	struct ZoneDrawer
	{
		/// Output image
		cv::Mat imageData;
		RNS::MapInfo mapInfo;

		static int callback(RNS::Zone * zone, ZoneDrawer * zd)
		{
			drawZone(zd->mapInfo, zone, zd->imageData, Scalar(40, 40, 200));
			return 1;
		}
	};

	// Create OpenCV window
	namedWindow("Display Image", CV_WINDOW_AUTOSIZE );

	ZoneDrawer drawer;

	bool hasZones = false;

	/// Read until timeout or error occurs
	while(waitKey(1))
	{
		/// Request map from server
		if(navigator.requestMap() != RNS::CommandDone)
			continue;

		if(!hasZones && editor.pull() == RNS::CommandDone)
		{
			printf("Obtained zone contents\n");
			hasZones = true;
		}

		RNS::MapInfo & mapInfo = drawer.mapInfo;
		/// Getting information about map size and resolution
		navigator.getMapInfo(mapInfo);
		cv::Mat mapData(mapInfo.height, mapInfo.width, CV_8UC1);
		navigator.getOccupancy((signed char*)mapData.data);

		cv::cvtColor(mapData, drawer.imageData, CV_GRAY2RGB);

		float robotPose[3];
		/// Draw robot pose
		if (navigator.getRobotPose(robotPose))
		{
			float coords[2];
			convert2map(mapInfo, robotPose, coords);
			circle(drawer.imageData, Point(coords[0], coords[1]), (0.2 / mapInfo.resolution) , Scalar(50,50,50));
		}

		if(hasZones)
		{
			editor.getZones((RNS::ZoneEditor::ZoneCallback)ZoneDrawer::callback, &drawer);
		}

		/*
		/// Draw laser beams
		RNS::RangerInfo rangerInfo;
		if(sensors.getRangerInfo(&rangerInfo))
		{
			float sensorWorldPose[3];
			/// Calculate world position of laser sensor
			sum_poses(robotPose, rangerInfo.pose, sensorWorldPose);
			const float * ranges = sensors.getRanges();
			float resolution = (rangerInfo.angleMax - rangerInfo.angleMin) / rangerInfo.beams;
			/// Draw laser points
			for(int i = 0; i < rangerInfo.beams; i++)
			{
				float r = ranges[i];
				float angle = rangerInfo.angleMin + sensorWorldPose[2] + resolution*i;
				if(r < rangerInfo.rangeMin || r >= rangerInfo.rangeMax)
					continue;

				/// Laser beam position in world coordinates
				float coords[2] =
				{
					sensorWorldPose[0] + cos(angle) * r,
					sensorWorldPose[1] + sin(angle) * r,
				};
				/// Converting to map coordinates
				convert2map(mapInfo, coords, coords);
				circle(imageData, Point(coords[0], coords[1]), (0.2 / mapInfo.resolution) , Scalar(255,50,50));
			}
		}*/

		cv::imshow("Display Image", drawer.imageData);
	}
	/// everything is done. Can exit. All internal data would be automatically released
	return 0;
}

