
/** \example move_quad
 * This is an example of how to use the Test class.
 * More details about this example.
 */
#include "stdio.h"
#include <rns_client/Clients.h>

/// This example connects to robot server and commands robot to
int main(int argc, char * argv[])
{
	const char * robotAddress = argc > 1 ? argv[1] : "localhost";

	RNS::Connector connector(robotAddress);
	RNS::ChassisClient client(connector);

	/// move robot to point (2,0) and wait until completion
	/*
	client.drive(0.2, 0, 0);
	client.sleep(2000);
	client.drive(-0.2, 0, 0);
	client.sleep(2000);
	client.drive(0, 0, 0);*/
	client.setTargetFrame(RNS::ChassisClient::FrameRobotCurrent);

	client.sleep(2000);
	client.moveto(1.4,0.0);
	client.sleep(2000);
	client.moveto(-1.4,0.0);
	/*
	client.moveto(0.0,0.3);
	client.moveto(0.0,0.3);
	client.moveto(0.0,0.3);
	client.moveto(2,2);
	client.moveto(0,2);
	client.moveto(0,0);*/
	/// everything is done. Can exit. All internal data would be automatically released
	printf("Exiting\n");
	return 0;
}

