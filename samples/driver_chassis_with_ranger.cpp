/*
 * driver_chassis_with_ranger.cpp
 *
 *  Created on: May 7, 2014
 *      Author: Dmitry Kargin
 *
 *  This example shows how to fill in chassis and sensor data and send it to navigation server
 */


#include "rns_client/ClientDriver.h"
#include <iostream>

using std::cout;

using RNS::Joystick;
using RNS::RobotDriverClient;

int main(int argc, char * argv[])
{
	// Target velocity data variable  
	Joystick joy;

	// Examples of server address types:
	//RobotDriverClient clientDriver("tcp://10.5.5.1:6666"); // communicating by IP
	//RobotDriverClient clientDriver("tcp://notemind02:6666"); // communicating by hostname
	RobotDriverClient clientDriver("tcp://localhost:6666"); // communication with the same computer

	// infinite loop stopper
	bool done = false;

	while (!done)
	{
		// if any data was received print int on screen
		// Waits wait delay time (50ms by default) and goes on.
		if (clientDriver.receive(joy))
		{
			cout << "joystick  x: " << joy.x_speed << " y: "  << joy.y_speed << " angle: " << joy.angle_speed << "\n";
			// Execute velocity commands here
			// ...
		}
		// 1. Fill in and send odometry data
		{
			RNS::Odometry odom;
			// Should fill odom structure here
			// ...
			clientDriver.send(odom);
		}
		// 2. Fill in and send laser scanner data
		{
			double * ranges = new double[MaxLaserRanges];
			RNS::LaserInfo laserInfo;
			// Should fill laser info structure here
			// ...
			clientDriver.send(laserInfo, ranges);
			delete []ranges;
		}
	}

	return 0;
}

