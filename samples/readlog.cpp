/*
 * test_odom.cpp
 *
 *  Created on: May 15, 2014
 *      Author: vrobot
 */

#include "stdio.h"
#include <rns_client/Clients.h>

/** \example readlog.cpp
 * Shows how to read debug message stream
 */
int main(int argc, char * argv[])
{
	/// specify DNS name or IP address
	const char * robotAddress = argc > 1 ? argv[1] : "localhost";

	/// internal network routines
	RNS::Connector connector(robotAddress);
	RNS::SystemClient client(connector);

	/// Read until timeout or error occurs
	while(true)
	{
		RNS::LogMessage msg;

		if(client.getLog(msg))
		{
			printf("LOG: %s\n", msg.message.c_str());
		}
	}
	/// everything is done. Can exit. All internal data would be automatically released
	return 0;
}

