/*
 * driver_chassis.cpp
 *
 *  Created on: May 7, 2014
 *      Author: vrobot
 */


#include "rns_client/ClientDriver.h"
#include <iostream>

using std::cout;
using RNS::Odometry;
using RNS::Joystick;
using RNS::RobotDriverClient;

int main(int argc, char * argv[])
{

	// Odometry data variable  
        Odometry odom;
	// Target velocity data variable  
        Joystick joy;

	// Examples of server address types:
        //RobotDriverClient clientDriver("tcp://10.11.12.13:6666"); // communicating by IP
        //RobotDriverClient clientDriver("tcp://notemind02:6666"); // communicating by hostname 
        RobotDriverClient clientDriver("tcp://localhost:6666"); // communication with the same computer

	// infinite loop stopper
        bool done = false;

        int imax = 1000;
        int i = 0;

        while (!done) {

		// if any data was received print int on screen
		// Waits wait delay time (50ms by default) and goes on.
                if (clientDriver.receive(joy)) {
                        cout << "joystick  x: " << joy.x_speed << " y: "  << joy.y_speed << " angle: " << joy.angle_speed << "\n";
                }

		// if new data is available (i % 2 == 0) send it to server, else resend last data
                if (i % 2 == 0) {
			// generating some fake data
                        odom.x = (i / (float) imax); // forward movement
                        odom.y = (2 * i / (float) imax); // strafe movemant. If robot kinematics doesn't allow strafe  set to zero
                        odom.angle = (3 * i / (float) imax); // rotation of the robot

                        clientDriver.send(odom); // send filled data. Waits wait delay time (50ms by default) and goes on.
                } else {
                        clientDriver.resend(); // if no data to resend exits instantly
                }

                i++;
                i %= imax;
	}

	return 0;
}

