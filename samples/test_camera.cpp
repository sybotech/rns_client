/*
 * test_odom.cpp
 *
 *  Created on: May 15, 2014
 *      Author: vrobot
 */

#include <rns_client/Clients.h>
#include <opencv2/opencv.hpp>
#include <stdio.h>

/** \example test_sensor.cpp
 * This is an example of how read laser rangefinder data from robot
 * @section Setting Up
 * - Connect rangedinger to controller
 * - Power on device
 * - Use configurator to enable appropriate sensor driver
 */
/// This example connects to robot server and commands robot to
int main(int argc, char * argv[])
{
	/// specify DNS name or IP address
	const char * robotAddress = argc > 1 ? argv[1] : "localhost";

	/// internal network routines
	RNS::Connector connector(robotAddress);
	RNS::SensorClient client(connector);

	// Create OpenCV window
	cv::namedWindow("Display Image", CV_WINDOW_AUTOSIZE );
	/// Read until timeout or error occurs
	while(true)
	{
		float odom[3];
		/// waiting 100ms to get any data
		client.wait(100);

		RNS::CameraInfo ci;
		if(client.getCameraInfo(&ci))
		{
			//printf("Got sensor data: amin=%f; amax=%f; rmax=%f; beams=%d\n", ri.angleMin, ri.angleMax, ri.rangeMax, ri.beams);
			//printf("Laser is mounted at: %f;%f;%f\n", ri.pose[0], ri.pose[1], ri.pose[2]);

			cv::Mat mapData(ci.height, ci.width, CV_8UC3);
			client.getCameraData(mapData.data);
			cv::imshow("Display Image", mapData);
		}
		cv::waitKey(1);
	}

	return 0;
}

