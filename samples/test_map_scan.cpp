/** \example test_odom.cpp
 *
 *  Created on: Jun 9, 2014
 *
 *      Author: Dmitry Kargin
 *      This example shows handling with map data from navigation system
 */

#include <rns_client/Clients.h>
#include <opencv2/opencv.hpp>
#include <stdio.h>

using namespace cv;

/// Converts from world coordinates to pixel coordinates
void convert2map(const RNS::MapInfo &mf, float from[2], float to[2])
{
	to[0] = (from[0] - mf.pose[0])/mf.resolution;
	to[1] = (from[1] - mf.pose[1])/mf.resolution;
}

/// Add two 2d orientations, specified by [x; y; angle] triplet
void sum_poses(float pose_a[3], float pose_b[3], float result[3])
{
	float cs = cos(pose_a[2]);
	float sn = sin(pose_a[2]);

	result[0] = pose_a[0] + cs * pose_b[0] - sn * pose_b[1];
	result[1] = pose_a[1] + sn * pose_b[0] + cs * pose_b[1];
	result[2] = pose_a[2] + pose_b[2];
}

int main(int argc, char * argv[])
{
	/// specify DNS name or IP address
	const char * robotAddress = argc > 1 ? argv[1] : "localhost";

	RNS::Connector connector(robotAddress);
	RNS::NavigatorClient navigator(connector);
	RNS::SystemClient sysClient(connector);
	RNS::SensorClient sensors(connector);

	// Create OpenCV window
	namedWindow("Display Image", CV_WINDOW_AUTOSIZE );
	/// Output image
	cv::Mat imageData;

	/// Read until timeout or error occurs
	while(waitKey(1))
	{
		/// Request map from server
		if(navigator.requestMap() != RNS::CommandDone)
			continue;

		RNS::MapInfo mapInfo;
		/// Getting information about map size and resolution
		navigator.getMapInfo(mapInfo);
		cv::Mat mapData(mapInfo.height, mapInfo.width, CV_8UC1);
		navigator.getOccupancy((signed char*)mapData.data);

		cv::cvtColor(mapData, imageData, CV_GRAY2RGB);

		float robotPose[3];
		/// Draw robot pose
		if (navigator.getRobotPose(robotPose)) {
			float coords[2];
			convert2map(mapInfo, robotPose, coords);
			circle(imageData, Point(coords[0], coords[1]), (0.2 / mapInfo.resolution) , Scalar(50,50,50));
		}
		/// Draw laser beams
		RNS::RangerInfo rangerInfo;
		if(sensors.getRangerInfo(&rangerInfo))
		{
			float sensorWorldPose[3];
			/// Calculate world position of laser sensor
			sum_poses(robotPose, rangerInfo.pose, sensorWorldPose);
			const float * ranges = sensors.getRanges();
			float resolution = (rangerInfo.angleMax - rangerInfo.angleMin) / rangerInfo.beams;
			/// Draw laser points
			for(int i = 0; i < rangerInfo.beams; i++)
			{
				float r = ranges[i];
				float angle = rangerInfo.angleMin + sensorWorldPose[2] + resolution*i;
				if(r < rangerInfo.rangeMin || r >= rangerInfo.rangeMax)
					continue;

				/// Laser beam position in world coordinates
				float coords[2] =
				{
					sensorWorldPose[0] + cosf(angle) * r,
					sensorWorldPose[1] + sinf(angle) * r,
				};
				/// Converting to map coordinates
				convert2map(mapInfo, coords, coords);
				circle(imageData, Point(coords[0], coords[1]), (0.2 / mapInfo.resolution) , Scalar(255,50,50));
			}
		}

		cv::imshow("Display Image", imageData);
	}
	/// everything is done. Can exit. All internal data would be automatically released
	return 0;
}

