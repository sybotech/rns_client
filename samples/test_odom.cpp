/*
 * test_odom.cpp
 *
 *  Created on: May 15, 2014
 *      Author: vrobot
 */

#include "stdio.h"
#include <rns_client/Clients.h>

/** \example test_odom.cpp
 * This is an example of how to use the Test class.
 * More details about this example.
 */
/// This example connects to robot server and commands robot to
int main(int argc, char * argv[])
{
	/// specify DNS name or IP address
	const char * robotAddress = argc > 1 ? argv[1] : "localhost";

	/// internal network routines
	RNS::Connector connector(robotAddress);

	RNS::ChassisClient client(connector);

	const int readDelay = 500;	// 10ms
	/// Read until timeout or error occurs
	while(true)
	{
		float odom[3];
		/*
		if(client.wait(readDelay) == RNS::CommandTimeout)
		{
			printf("Timeout occured\n");
		}
		else */if(client.getOdometry(odom))
		{
			printf("Got new odometry reading: %f;%f;%f\n", odom[0], odom[1], odom[2]);
		}
	}
	/// everything is done. Can exit. All internal data would be automatically released
	return 0;
}

