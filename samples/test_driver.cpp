/*
 * test_driver.cpp
 *
 *  Created on: May 7, 2014
 *      Author: vrobot
 */

/// Sends velocity command to actual hardware. Implement it yourself
void executeVelocityCommand(float vel[3]);
/// Reads odometry from actual hardware. Implement it yourself
void readOdometry(float odom[3]);

namespace RNS
{
	/// Does all connection-relates stuff
	class ChassisDriver
	{
	public:
		ChassisDriver(const char * robotAddress);
		~ChassisDriver();
		/// get last error message
		const char * getError() const;
		/// Check if driver is connected to navigation server
		bool isConnected() const;
		/// wait until any data arrives
		int waitData(int ms);
		/// get last velocity command {x,y, angle}
		int getVelocityCmd(float velocity[3]);
		/// set last odometry reading. It is sent immediately. Values are {x,y,angle, vx, vy, va}
		int setOdometry(float odom[6]);
		/// set last battery state
		int setBattery(float current, float max, bool charing);
	};
}

int main(int argc, char * argv[])
{
	const char * robotAddress = "robotURL:7777";
	RNS::ChassisDriver driver(robotAddress);

	bool done = false;
	double waitDelay = 0.1;	/// 100 ms delay period

	while(!done)
	{
		/// read odometry from hardware and prepare for sending
		float odom[3];
		readOdometry(odom);
		driver.setOdometry(odom);
		/// wait for any velocity commands
		if(driver.waitData(waitDelay))
		{
			float vel[3];
			if(driver.getVelocityCmd(vel))
				executeVelocityCommand(vel);
		}
	}
	return 0;
}
