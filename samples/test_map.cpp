/** \example test_odom.cpp
 *
 *  Created on: Jun 9, 2014
 *
 *      Author: Dmitry Kargin
 *      This example shows handling with map data from navigation system
 */

#include <rns_client/Clients.h>
#include <opencv2/opencv.hpp>
#include <stdio.h>

using namespace cv;

int main(int argc, char * argv[])
{
	/// specify DNS name or IP address
	const char * robotAddress = argc > 1 ? argv[1] : "localhost";

	RNS::Connector connector(robotAddress);
	RNS::NavigatorClient client(connector);
	RNS::SystemClient sysClient(connector);

	// Create OpenCV window
	namedWindow("Display Image", CV_WINDOW_AUTOSIZE );

	/// Read until timeout or error occurs
	while(true)
	{
		/// Request map from server
		if(client.requestMap() == RNS::CommandDone)
		{
			RNS::MapInfo mapInfo;
			/// Getting information about map size and resolution
			if(client.getMapInfo(mapInfo))
			{
				printf("Got map contents: %dx%dx%fm\n", mapInfo.width, mapInfo.height, mapInfo.resolution);
				cv::Mat mapData(mapInfo.height, mapInfo.width, CV_8UC1);

				client.getOccupancy((signed char*)mapData.data);
				cv::imshow("Display Image", mapData);
			}
		}
		waitKey(1);
	}
	/// everything is done. Can exit. All internal data would be automatically released
	return 0;
}

