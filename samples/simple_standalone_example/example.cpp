

#include <iostream>
#include <sstream>

#include <stdlib.h>
#include <unistd.h>
#include <cstring>

#include <messages.pb.h>
#include <zmq.hpp>

/// TODO: get rid of <zmq.hpp>

int main()
{
	zmq::context_t context(1);

	zmq::socket_t subscriber(context, ZMQ_SUB);
	//char connectionString[] = "tcp://192.168.2.164:5557";
//#define address "tcp://192.168.2.164"
#define address "tcp://127.0.0.1"
	char connectionString[] = address":5557";
	subscriber.connect(connectionString);

	zmq::socket_t broadcast(context, ZMQ_PUSH);
	char broadcastConnectionString[] = address":5558";
	broadcast.connect(broadcastConnectionString);

	char header = zmqTypes::PacketTicketState;
	zmq_setsockopt(subscriber, ZMQ_SUBSCRIBE, &header, sizeof(header));

	char headerState = zmqTypes::PacketState;
	zmq_setsockopt(subscriber, ZMQ_SUBSCRIBE, &headerState, sizeof(headerState));

	std::cout << "Beginning to get messages from " << connectionString << " ..." << std::endl;

	for (int update_nbr = 0; /*update_nbr < 100*/ ; update_nbr++) {

		zmq::message_t update;
		subscriber.recv(&update);
		char *dataPtr = static_cast<char*>(update.data());
		char key = dataPtr[0];
		ssize_t size  = update.size();

		std::cout << "Got message!" << std::endl;

		if(key == zmqTypes::PacketTicketState)
		{
		    zmqTypes::TicketInfo msg;
		    if(msg.ParseFromString(std::string(dataPtr+1, size-1)))
		    {
			if(msg.type() == "mover" && msg.tag() == "table" && msg.state() == zmqTypes::TicketDone)
			{
			    /// We've reached the table
			    /// TODO: do something related to it
			    std::cout << "Got to table" << std::endl;

			    sleep(3);

			    char broadcastHeader = zmqTypes::PacketBroadcast;
			    zmqTypes::Broadcast broadcastMessage;
			    broadcastMessage.set_dest("interface");
			    broadcastMessage.set_data("gohome");
			    broadcastMessage.set_tag("mover");
			    std::string broadcastData = broadcastHeader + broadcastMessage.SerializeAsString();

			    zmq::message_t broadcastZmqMessage(broadcastData.size());
			    std::memcpy(broadcastZmqMessage.data(), broadcastData.c_str(), broadcastData.size());

			    broadcast.send(broadcastZmqMessage);

			    std::cout << "Broadcast sent" << std::endl;
			}
			else if(msg.type() == "mover" && msg.tag() == "home" && msg.state() == zmqTypes::TicketDone)
			{
			    /// We've reached the home location
			    /// TODO: do something related to it
			    std::cout << "Got home" << std::endl;
			} else {
			    std::cout << "Got somewhere" << std::endl;
			    std::cout << "msg.type()=" << msg.type() 
			              << " msg.tag()=" << msg.tag() 
			              << " (msg.state() == zmqTypes::TicketDone)=" << (msg.state() == zmqTypes::TicketDone) << std::endl;
			}
		    }
		}
		else if(key == zmqTypes::PacketState) 
		{
			std::cout << " ! Got state." << std::endl;
		}


		//std::istringstream iss(static_cast<char*>(update.data()));
		//iss >> zipcode >> temperature >> relhumidity ;

		//total_temp += temperature;

    }

	return 0;
}

/*
socket.setsockopt(zmq.SUBSCRIBE, chr(messages_pb2.PacketTicketState))PacketTicketState

ticketState = messages_pb2.TicketState()
ticketSTate.ParseFromString(msgData)

broadcast = messages_pb2.Broadcast()
broadcast.dest = "interface"
broadcast.data = "gohome"
broadcast.tag = "mover"

messages_pb2.PacketBroadcast
*/
