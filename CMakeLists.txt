project(RNSClient)
cmake_minimum_required(VERSION 2.6)

set(RNS_CLIENT_VERSION_MAJOR "1")
set(RNS_CLIENT_VERSION_MINOR "3")
set(RNS_CLIENT_VERSION_PATCH "1")

set(RNS_CLIENT_VERSION "${RNS_CLIENT_VERSION_MAJOR}.${RNS_CLIENT_VERSION_MINOR}.${RNS_CLIENT_VERSION_PATCH}")

if(WIN32 AND NOT CYGWIN)
  set(DEF_INSTALL_CMAKE_DIR CMake)
else()
  set(DEF_INSTALL_CMAKE_DIR lib/cmake/${PROJECT_NAME})
endif()

set(INSTALL_LIB_DIR lib)# CACHE PATH "Installation directory for libraries")
set(INSTALL_BIN_DIR bin)# CACHE PATH "Installation directory for executables")
set(INSTALL_INCLUDE_DIR include)# CACHE PATH "Installation directory for header files")
set(INSTALL_SHARE_DIR share)# CACHE PATH "Installation directory for header files")
set(INSTALL_CMAKE_DIR ${DEF_INSTALL_CMAKE_DIR})# CACHE PATH "Installation directory for CMake files")

# Make relative paths absolute (needed later on)
foreach(p LIB BIN INCLUDE CMAKE SHARE)
  set(var INSTALL_${p}_DIR)
  if(NOT IS_ABSOLUTE "${${var}}")
    set(${var} "${CMAKE_INSTALL_PREFIX}/${${var}}")
  endif()
endforeach()

#set the default path for built executables to the "bin" directory
set(EXECUTABLE_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/bin)
#set the default path for built libraries to the "lib" directory
set(LIBRARY_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/lib)

LIST(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/cmake/")

message(STATUS "CMake module path = " ${CMAKE_MODULE_PATH})

include(FindProtobuf)
find_package(Protobuf REQUIRED)

include_directories(src include)

set(PROTO_MSG_FILE ${PROJECT_SOURCE_DIR}/zmqmsg/messages.proto)

option(RNSCLIENT_BUILD_BINDINGS "Build bingings using SWIG" OFF)

add_subdirectory(src)

option(RNSCLIENT_BUILD_SAMPLES "Build RNS examples" OFF)

if(RNSCLIENT_BUILD_SAMPLES)
	add_subdirectory(samples)
endif(RNSCLIENT_BUILD_SAMPLES)

#-- Add an Option to toggle the generation of the API documentation
option(RNSCLIENT_BUILD_DOCUMENTATION "Use Doxygen to create the HTML based API documentation" OFF)
if(RNSCLIENT_BUILD_DOCUMENTATION)
  FIND_PACKAGE(Doxygen)
  if (NOT DOXYGEN_FOUND)
    message(FATAL_ERROR "Doxygen is needed to build the documentation. Please install it correctly")
  endif()
  #-- Configure the Template Doxyfile for our specific project
  configure_file(Doxyfile.in ${PROJECT_BINARY_DIR}/Doxyfile  @ONLY IMMEDIATE)
  #-- Add a custom target to run Doxygen when ever the project is built
  add_custom_target (Docs ALL	COMMAND ${DOXYGEN_EXECUTABLE} ${PROJECT_BINARY_DIR}/Doxyfile
  								SOURCES ${PROJECT_BINARY_DIR}/Doxyfile)
  								
  install(DIRECTORY ${PROJECT_SOURCE_DIR}/docs DESTINATION share COMPONENT doc)
endif()

install(DIRECTORY include/rns_client DESTINATION include COMPONENT SDK)

if(RNSCLIENT_BUILD_SAMPLES)
	install(DIRECTORY samples DESTINATION share COMPONENT samples)
endif(RNSCLIENT_BUILD_SAMPLES)

install(DIRECTORY zmqmsg DESTINATION share COMPONENT SDK)

# The interesting stuff goes here
# ===============================
 
# Add all targets to the build-tree export set
#export(TARGETS rns_client rns_driver FILE "${PROJECT_BINARY_DIR}/RNSClientTargets.cmake") 
# Export the package for use from the build-tree
# (this registers the build-tree with a global CMake-registry)
#export(PACKAGE RNSClient)

# Create the RNSClientConfig.cmake and RNSClientConfigVersion files

#file(RELATIVE_PATH REL_INCLUDE_DIR "${INSTALL_CMAKE_DIR}" "${INSTALL_INCLUDE_DIR}")


file(RELATIVE_PATH REL_SHARE_DIR "${INSTALL_CMAKE_DIR}" "${INSTALL_SHARE_DIR}")

#message(STATUS "install Cmake=${INSTALL_CMAKE_DIR}")
#message(STATUS "install share=${INSTALL_SHARE_DIR}")
#message(STATUS "install rel share=${REL_SHARE_DIR}")

set(CONF_SHARE_DIR "${CMAKE_INSTALL_PREFIX}/share")
set(CONF_LIB_DIR "${CMAKE_INSTALL_PREFIX}/lib")

#set(CONF_INCLUDE_DIRS "${PROJECT_SOURCE_DIR}" "${PROJECT_BINARY_DIR}")
# configure_file(RNSClientConfig.cmake.in "${PROJECT_BINARY_DIR}/RNSClientConfig.cmake" @ONLY)
# ... for the install tree

#set(CONF_INCLUDE_DIRS "${RNSCLIENT_CMAKE_DIR}/${REL_INCLUDE_DIR}")
set(CONF_INCLUDE_DIRS "${CMAKE_INSTALL_PREFIX}/include")

file(RELATIVE_PATH REL_SHARE_DIR "${INSTALL_CMAKE_DIR}" "${CONF_SHARE_DIR}")
set(CONF_SHARE_DIR "${REL_SHARE_DIR}")


configure_file(RNSClientConfig.cmake.in "${PROJECT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/RNSClientConfig.cmake" @ONLY)
# ... for both
configure_file(RNSClientConfigVersion.cmake.in "${PROJECT_BINARY_DIR}/RNSClientConfigVersion.cmake" @ONLY)
 
# Install the RNSClientConfig.cmake and RNSClientConfigVersion.cmake
install(FILES
  "${PROJECT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/RNSClientConfig.cmake"
  "${PROJECT_BINARY_DIR}/RNSClientConfigVersion.cmake"
  DESTINATION "${INSTALL_CMAKE_DIR}" COMPONENT dev)

# Install the export set for use with the install-tree
#install(EXPORT RNSClientTargets DESTINATION "${INSTALL_CMAKE_DIR}" COMPONENT dev)

# uninstall target
configure_file(
    "${CMAKE_CURRENT_SOURCE_DIR}/cmake_uninstall.cmake.in"
    "${CMAKE_CURRENT_BINARY_DIR}/cmake_uninstall.cmake"
    IMMEDIATE @ONLY)

add_custom_target(uninstall COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/cmake_uninstall.cmake)

# CPack installation
set(CPACK_PACKAGE_NAME "RNS Client")
set(CPACK_PACKAGING_INSTALL_PREFIX ${CMAKE_INSTALL_PREFIX})
set(CPACK_PACKAGE_VENDOR "SyboTech LLC")
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "RNS Client libraries.\n Contains headers and libraries to develop C++ applications.")

set(CPACK_PACKAGE_VERSION_MAJOR ${RNS_CLIENT_VERSION_MAJOR})
set(CPACK_PACKAGE_VERSION_MINOR ${RNS_CLIENT_VERSION_MINOR})
set(CPACK_PACKAGE_VERSION_PATCH ${RNS_CLIENT_VERSION_PATCH})
set(CPACK_PACKAGE_VERSION "${CPACK_PACKAGE_VERSION_MAJOR}.${CPACK_PACKAGE_VERSION_MINOR}.${CPACK_PACKAGE_VERSION_PATCH}")
#set(CPACK_PACKAGE_INSTALL_DIRECTORY "CPack Component Example")
set(CPACK_PACKAGE_DESCRIPTION_FILE "${CMAKE_SOURCE_DIR}/description.txt")
set(CPACK_PACKAGE_FILE_NAME "rnsclient")
set(CPACK_RESOURCE_FILE_LICENSE "${CMAKE_SOURCE_DIR}/licence.txt")
set(CPACK_PACKAGE_FILE_NAME "rnsclient-${CPACK_PACKAGE_VERSION}")
set(CPACK_COMPONENT_SDK_DISPLAY_NAME "SDK Contents")
set(CPACK_COMPONENT_SAMPLES_DISPLAY_NAME "Examples")

# for debian
SET(CPACK_DEBIAN_PACKAGE_NAME "librnsclient-dev")
SET(CPACK_DEBIAN_PACKAGE_DEPENDS "libprotobuf-dev")
SET(CPACK_DEBIAN_PACKAGE_MAINTAINER "Dmitry Kargin dv_frost@mail.ru")
SET(CPACK_DEBIAN_PACKAGE_PROVIDES "librnsclient-dev")

# for NSIS 
set(CPACK_NSIS_MODIFY_PATH ON)
set(CPACK_NSIS_DISPLAY_NAME "RNS Client Libraries")
include(CPack)
