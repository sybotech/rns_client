# README

RNS Client SDK is C++ library for simplifying interaction with SBrick navigation computer.

## Installation
RNS Client SDK uses [ZMQ v3.2](https://github.com/zeromq/zeromq3-x/releases/download/v3.2.5/zeromq-3.2.5.tar.gz)  and protobuf v8. These libraries should be installed first

**Warning:**

```
#!bash

sudo apt install libzmq-dev
```
would not work (it will install zmq 2.2.0)

## Building 

Download source package to any available folder and unpack it. Then execute the following commands in the shell:
```
#!bash

sudo apt-get install libprotobuf-dev protobuf-compiler 

cd rns_client;
mkdir build;
cd build;
cmake ..;
make;
sudo make install;
```
Development files are installed to /usr/local/include and /usr/local/lib directories. You can edit install directory by setting CMAKE_INSTALL_PATH variable. Refer to cmake documentation.